package com.bet.util.multisocket.common.handler;

import com.bet.util.multisocket.common.handler.AbstractIdleChannelHandler;
import com.bet.util.multisocket.common.handler.IdleHandlerContext;
import com.bet.util.multisocket.common.heartbeat.HeartbeatEventListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;

/**
 * User: istvan.illyes
 * Date: 7/1/13
 */
public final class PinglessIdleChannelHandler extends AbstractIdleChannelHandler {

    PinglessIdleChannelHandler(final HeartbeatEventListener stateEvent, final String socketId,
                               final String clientId, final IdleHandlerContext context) {
        super(stateEvent, socketId, clientId, context);
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent e = (IdleStateEvent) evt;
            if (e.state().equals(IdleState.WRITER_IDLE)) {
                // do nothing

            } else {
                super.userEventTriggered(ctx, evt);
            }
        }
    }
}
