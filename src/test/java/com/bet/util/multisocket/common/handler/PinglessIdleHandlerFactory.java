package com.bet.util.multisocket.common.handler;

import com.bet.util.multisocket.common.heartbeat.HeartbeatEventListener;

/**
 * User: istvan.illyes
 * Date: 2/5/14
 */
public final class PinglessIdleHandlerFactory extends AbstractIdleHandlerFactory {

    @Override
    public AbstractIdleHandler create(
            int timeoutMillis,
            HeartbeatEventListener heartbeatEventListener,
            String clientId
    ) {
        return new PinglessIdleHandler(timeoutMillis, heartbeatEventListener, clientId);
    }
}
