package com.bet.util.multisocket.server;

import com.bet.util.multisocket.common.handler.PinglessIdleHandlerFactory;
import com.bet.util.multisocket.common.endpoint.EndpointListener;

/**
 * User: istvan.illyes
 * Date: 2/5/14
 */
public final class PinglessServerFactory extends AbstractServerFactory {

    public PinglessServerFactory() {
        super(new PinglessIdleHandlerFactory());
    }

    @Override
    public Server create(ServerContext serverContext, EndpointListener<ServerEndpoint> endpointListener) {
        return new Server(serverContext, endpointListener, getIdleHandlerFactory());
    }
}
