package com.bet.util.multisocket.integration;

import java.net.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

import com.bet.util.multisocket.client.AbstractClient;
import com.bet.util.multisocket.client.ClientContext;
import com.bet.util.multisocket.client.ClientFactory;
import com.bet.util.multisocket.client.ManualReconnectingClient;
import com.bet.util.multisocket.client.ManualReconnectingClientEndpoint;
import com.bet.util.multisocket.common.ChannelType;
import com.bet.util.multisocket.common.InvalidStateException;
import com.bet.blues.proxy.socket.multi.SubjectsRouting;
import com.bet.util.multisocket.server.AbstractServerFactory;
import com.bet.util.multisocket.server.PinglessServerFactory;
import com.bet.util.multisocket.server.Server;
import com.bet.util.multisocket.server.ServerContext;
import com.bet.util.multisocket.server.ServerEndpoint;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * User: istvan.illyes
 * Date: 2/5/14
 */
public final class TestReconnectionManualReconnectingClient {

    private static final Logger LOGGER = Logger.getLogger(TestReconnectionManualReconnectingClient.class);
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(4);
    private static final int WAIT_TIMEOUT_MILLIS = 8000;
    // Configs
    private static final String IP = "127.0.0.1";
    private static final int NO_CLIENTS = 3;
    private static final String[] SUBJECTS = {"X.blues.order.X.TestGroup5.X"};
    private static final String ROUTING_TABLE = ".*blues.order.*.TestGroup5.* : Master, .*blues.order.*.TestGroup7.* : S2, .*blues.ps.* : S2";
    private static final String DEFAULT_ROUTE = "Master";
    private static final String CLIENT_ID = "ClientId";
    private static final SubjectsRouting subjectsRouting = createSubjectsRouting();
    private static final String MESSAGE = "COMMON_MESSAGE";
    private static final int PORT = 1235;
    private static final int PING_TIMEOUT_MILLIS = 2000;
    // Endpoint Listeners
    private static final List<ClientEndpointListenerTestImpl<ManualReconnectingClientEndpoint>> CLIENT_ENDPOINT_LISTENERS =
            new ArrayList<ClientEndpointListenerTestImpl<ManualReconnectingClientEndpoint>>(NO_CLIENTS);
    private static final ServerEndpointListenerTestImpl<ServerEndpoint> SERVER_ENDPOINT_LISTENER =
            new ServerEndpointListenerTestImpl<ServerEndpoint>(NO_CLIENTS);
    // Client
    private static final List<ManualReconnectingClient> CLIENTS = new ArrayList<ManualReconnectingClient>(NO_CLIENTS);
    private static final List<ManualReconnectingClientEndpoint> CLIENT_ENDPOINTS =
            new ArrayList<ManualReconnectingClientEndpoint>(NO_CLIENTS);
    // Server
    private static final ServerContext SERVER_CONTEXT = ServerContext.createBuilder()
            .subjectsRouting(subjectsRouting)
            .channelType(ChannelType.OIO)
            .localAddress(new InetSocketAddress(PORT))
            .build();
    // Server has to be pingless (not sending ping messages), so that heartbeat can become lost on client side
    private static final AbstractServerFactory SERVER_FACTORY = new PinglessServerFactory();
    private static final Server SERVER = SERVER_FACTORY.create(SERVER_CONTEXT, SERVER_ENDPOINT_LISTENER);
    private static final List<ServerEndpoint> SERVER_ENDPOINTS = new ArrayList<ServerEndpoint>(NO_CLIENTS);

    @BeforeClass
    public static void setUpClass() throws InvalidStateException, InterruptedException {
        BasicConfigurator.configure();

        // Start server
        SERVER.start();

        // Start clients
        createClients();
        checkIfClientsWereConnected();
    }

    @AfterClass
    public static void tearDownClass() throws InterruptedException {
        // Stop server
        SERVER.stop();

        // Close endpoints
        for (ManualReconnectingClientEndpoint clientEndpoint : CLIENT_ENDPOINTS) {
            clientEndpoint.close();
        }
        for (ServerEndpoint serverEndpoint : SERVER_ENDPOINTS) {
            serverEndpoint.close();
        }

        // Stop clients
        for(AbstractClient client : CLIENTS) {
            client.stop();
        }
    }

    private static SubjectsRouting createSubjectsRouting() {
        SubjectsRouting subjectsRouting = new SubjectsRouting();
        subjectsRouting.addRoutingInfo(SubjectsRouting.unpackRoutingTable(ROUTING_TABLE));
        subjectsRouting.setDefaultRoute(DEFAULT_ROUTE);
        return subjectsRouting;
    }

    @Test
    public void testReconnectionWhenHeartbeatLost() throws InterruptedException, InvalidStateException {
        CountDownLatch communicationChecksLatch = new CountDownLatch(4);

        checkCommunication(communicationChecksLatch);

        waitForHeartbeatLost();

        // when {@code null} is read, endpoint was disconnected so it can be closed
        for(ManualReconnectingClientEndpoint endpoint : CLIENT_ENDPOINTS) {
            while(endpoint.readMessage() != null);
            endpoint.close();
        }

        // execute reconnect
        for(ManualReconnectingClientEndpoint endpoint : CLIENT_ENDPOINTS) {
            endpoint.reconnect();
        }

        waitForReconnect();

        checkCommunication(communicationChecksLatch);

        assertTrue(communicationChecksLatch.await(10 * WAIT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS));
    }

    private void waitForHeartbeatLost() throws InterruptedException {
        final CountDownLatch heartbeatLostLatch = new CountDownLatch(NO_CLIENTS);
        for(final ClientEndpointListenerTestImpl clientEndpointListener : CLIENT_ENDPOINT_LISTENERS) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        clientEndpointListener.waitForHeartbeatLost();
                        heartbeatLostLatch.countDown();
                        LOGGER.info("Heartbeat lost.");
                    } catch (InterruptedException e) {
                        LOGGER.warn("Failed to wait for heartbeat lost", e);
                    }
                }
            }).start();
        }
        heartbeatLostLatch.await();
        LOGGER.info("Wait for heartbeat lost ended.");
    }

    private void waitForReconnect() throws InterruptedException {
        final CountDownLatch reconnectLatch = new CountDownLatch(NO_CLIENTS);
        for(final ClientEndpointListenerTestImpl clientEndpointListener : CLIENT_ENDPOINT_LISTENERS) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        clientEndpointListener.waitForReconnect();
                        reconnectLatch.countDown();
                        LOGGER.info("Client reconnected.");
                    } catch (InterruptedException e) {
                        LOGGER.warn("Failed to wait for reconnect", e);
                    }
                }
            }).start();
        }
        reconnectLatch.await();
        LOGGER.info("Wait for reconnect ended.");
    }

    private void checkCommunication(CountDownLatch communicationChecksLatch) throws InterruptedException {
        EXECUTOR_SERVICE.execute(checkServerToClientCommunication(communicationChecksLatch));
        EXECUTOR_SERVICE.execute(checkClientToServerCommunication(communicationChecksLatch));
    }

    private Runnable checkServerToClientCommunication(final CountDownLatch countDownLatch) throws InterruptedException {
        return new Runnable() {
            @Override
            public void run() {
                try {
                    int noSentMessages = trySendMessagesFromServerToClients();
                    if (checkNoOfMessagesReceivedByClients(noSentMessages)) {
                        countDownLatch.countDown();
                    }
                } catch (InterruptedException e) {
                    LOGGER.error("Failed to check server to client communication", e);
                }
            }
        };
    }

    private int trySendMessagesFromServerToClients() throws InterruptedException {
        final AtomicInteger noSentMessages = new AtomicInteger(0);
        for (ServerEndpoint serverEndpoint : SERVER_ENDPOINTS) {
            for (String subject : SUBJECTS) {
                try {
                    serverEndpoint.sendMessage(MESSAGE, subject);
                    noSentMessages.incrementAndGet();
                } catch (Exception ex) {
                    LOGGER.error("Failed to send message.", ex);
                }
            }
        }
        assertEquals(SERVER_ENDPOINTS.size() * SUBJECTS.length, noSentMessages.get());
        return noSentMessages.get();
    }

    private boolean checkNoOfMessagesReceivedByClients(final int noSentMessages) throws InterruptedException {
        final AtomicInteger noReceivedMessages = new AtomicInteger(0);
        final Future future = EXECUTOR_SERVICE.submit(
                new Runnable() {
                    @Override
                    public void run() {
                        for (final ManualReconnectingClientEndpoint clientEndpoint : CLIENT_ENDPOINTS) {
                            int receivedMessages = 0;
                            while (receivedMessages < noSentMessages / CLIENT_ENDPOINTS.size()) {
                                try {
                                    clientEndpoint.readMessage();
                                    noReceivedMessages.incrementAndGet();
                                    receivedMessages++;
                                } catch (InterruptedException e) {
                                    LOGGER.error("Failed to read message.", e);
                                    break;
                                }
                            }
                        }
                    }
                }
        );
        try {
            future.get(WAIT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            LOGGER.warn("Failed to execute message reading task.", e);
        }

        return noSentMessages == noReceivedMessages.get();
    }

    private Runnable checkClientToServerCommunication(final CountDownLatch countDownLatch) throws InterruptedException {
        return new Runnable() {
            @Override
            public void run() {
                try {
                    int noSentMessages = trySendMessagesFromClientsToServer();
                    if (checkNoOfMessagesReceivedByServer(noSentMessages)) {
                        countDownLatch.countDown();
                    }
                } catch (InterruptedException e) {
                    LOGGER.error("Failed to check client to server communication", e);
                }
            }
        };
    }

    private int trySendMessagesFromClientsToServer() throws InterruptedException {
        final AtomicInteger noSentMessages = new AtomicInteger(0);
        for (ManualReconnectingClientEndpoint clientEndpoint : CLIENT_ENDPOINTS) {
            for (String subject : SUBJECTS) {
                try {
                    clientEndpoint.sendMessage(MESSAGE, subject);
                    noSentMessages.incrementAndGet();
                } catch (Exception ex) {
                    LOGGER.error("Failed to send message.", ex);
                }
            }
        }
        assertEquals(CLIENT_ENDPOINTS.size() * SUBJECTS.length, noSentMessages.get());
        return noSentMessages.get();
    }

    private boolean checkNoOfMessagesReceivedByServer(final int noSentMessages) throws InterruptedException {
        final AtomicInteger noReceivedMessages = new AtomicInteger(0);
        final Future future = EXECUTOR_SERVICE.submit(
                new Runnable() {
                    @Override
                    public void run() {
                        for (final ServerEndpoint serverEndpoint : SERVER_ENDPOINTS) {
                            int receivedMessages = 0;
                            while (receivedMessages < noSentMessages / SERVER_ENDPOINTS.size()) {
                                try {
                                    serverEndpoint.readMessage();
                                    noReceivedMessages.incrementAndGet();
                                    receivedMessages++;
                                } catch (InterruptedException e) {
                                    LOGGER.error("Failed to read message.", e);
                                    break;
                                }
                            }
                        }
                    }
                }
        );
        try {
            future.get(WAIT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            LOGGER.warn("Failed to execute message reading task.", e);
        }

        return noSentMessages == noReceivedMessages.get();
    }

    private static void checkIfClientsWereConnected() throws InterruptedException {
        for(ClientEndpointListenerTestImpl<ManualReconnectingClientEndpoint> listener : CLIENT_ENDPOINT_LISTENERS) {
            CLIENT_ENDPOINTS.add(listener.getEndpoint());
        }

        for(int i = 0; i < NO_CLIENTS; i++) {
            SERVER_ENDPOINTS.add(SERVER_ENDPOINT_LISTENER.accept());
        }
    }

    private static void createClients() throws InvalidStateException {
        final ClientFactory clientFactory = new ClientFactory();
        for (int i = 0; i < NO_CLIENTS; i++) {
            CLIENT_ENDPOINT_LISTENERS.add(new ClientEndpointListenerTestImpl<ManualReconnectingClientEndpoint>());
            try {
                ClientContext clientContext = ClientContext.createBuilder()
                        .channelType(ChannelType.OIO)
                        .remoteAddress(new InetSocketAddress(InetAddress.getByName(IP), PORT))
                        .timeoutMillis(PING_TIMEOUT_MILLIS)
                        .clientId(CLIENT_ID + i)
                        .build();
                ManualReconnectingClient client = clientFactory.createManualReconnectingClient(clientContext, CLIENT_ENDPOINT_LISTENERS.get(i));
                CLIENTS.add(client);
                client.start();
            } catch (UnknownHostException e) {
                LOGGER.error("Following exception caught: " + e);
            }
        }
    }
}
