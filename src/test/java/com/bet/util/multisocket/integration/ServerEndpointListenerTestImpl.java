package com.bet.util.multisocket.integration;

import java.util.concurrent.*;

import com.bet.util.multisocket.common.endpoint.EndpointListener;
import org.apache.log4j.Logger;

/**
 * User: istvan.illyes
 * Date: 2/5/14
 */
final class ServerEndpointListenerTestImpl<T> implements EndpointListener<T> {

    private static final Logger LOGGER = Logger.getLogger(ServerEndpointListenerTestImpl.class);
    private static final int MAXIMUM_CLIENT_NUMBER = 10;
    private BlockingQueue<T> endpoints = new ArrayBlockingQueue<T>(MAXIMUM_CLIENT_NUMBER);
    private final CountDownLatch heartbeatDeadLatch;
    private final CountDownLatch connectionClosedLatch;

    public ServerEndpointListenerTestImpl(final int noClients) {
        heartbeatDeadLatch = new CountDownLatch(noClients);
        connectionClosedLatch = new CountDownLatch(noClients);
    }

    @Override
    public void connectionEstablished(T endpoint) {
        final boolean added = endpoints.offer(endpoint);
        LOGGER.info(String.format("Endpoint added to queue: %b", added));
    }

    @Override
    public void connectionFailed() {
        //TODO unimplemented method body
    }

    @Override
    public void connectionClosed() {
        connectionClosedLatch.countDown();
    }

    @Override
    public void reconnectionSuccessful() {
        //TODO unimplemented method body
    }

    @Override
    public void reconnectionFailed() {
        //TODO unimplemented method body
    }

    @Override
    public void notifyHeartbeatRecovered(String socketId, String clientId) {
        //TODO unimplemented method body
    }

    @Override
    public void notifyHeartbeatLate(String socketId, String clientId) {
        //TODO unimplemented method body
    }

    @Override
    public void notifyHeartbeatLost(String socketId, String clientId) {
        //TODO unimplemented method body
    }

    @Override
    public void notifyHeartbeatDead(String socketId, String clientId) {
        heartbeatDeadLatch.countDown();
    }

    public T accept() throws InterruptedException {
        return endpoints.take();
    }

    public void waitForHeartbeatDead() throws InterruptedException {
        heartbeatDeadLatch.await();
    }

    public void waitForConnectionClosed() throws InterruptedException {
        connectionClosedLatch.await();
    }

}
