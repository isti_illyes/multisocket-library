package com.bet.util.multisocket.integration;

import java.util.concurrent.*;

import com.bet.util.multisocket.common.endpoint.EndpointListener;

/**
 * User: istvan.illyes
 * Date: 2/5/14
 */
public class ClientEndpointListenerTestImpl<T> implements EndpointListener<T> {

    private static final int TIMEOUT = 10000;
    private T endpoint;
    private final CountDownLatch connectionEstablishedLatch = new CountDownLatch(1);
    private final CountDownLatch heartbeatLostLatch = new CountDownLatch(1);
    private CountDownLatch reconnectLatch = new CountDownLatch(1);

    @Override
    public void connectionEstablished(T endpoint) {
        this.endpoint = endpoint;
        connectionEstablishedLatch.countDown();
    }

    @Override
    public void connectionFailed() {
        //TODO unimplemented method body
    }

    @Override
    public void connectionClosed() {
        //TODO unimplemented method body
    }

    @Override
    public void reconnectionSuccessful() {
        reconnectLatch.countDown();
    }

    @Override
    public void reconnectionFailed() {
        //TODO unimplemented method body
    }

    @Override
    public void notifyHeartbeatRecovered(String socketId, String clientId) {
        //TODO unimplemented method body
    }

    @Override
    public void notifyHeartbeatLate(String socketId, String clientId) {
        //TODO unimplemented method body
    }

    @Override
    public void notifyHeartbeatLost(String socketId, String clientId) {
        heartbeatLostLatch.countDown();
    }

    @Override
    public void notifyHeartbeatDead(String socketId, String clientId) {
        //TODO unimplemented method body
    }

    public T getEndpoint() throws InterruptedException {
        connectionEstablishedLatch.await(TIMEOUT, TimeUnit.MILLISECONDS);
        return endpoint;
    }

    public void waitForHeartbeatLost() throws InterruptedException {
        heartbeatLostLatch.await();
    }

    public void waitForReconnect() throws InterruptedException {
        reconnectLatch.await();
        reconnectLatch = new CountDownLatch(1);
    }
}
