package com.bet.util.multisocket.client;

import com.bet.util.multisocket.common.handler.PinglessIdleHandlerFactory;

/**
 * User: istvan.illyes
 * Date: 2/5/14
 */
public final class PinglessClientFactory extends AbstractClientFactory {

    public PinglessClientFactory() {
        super(new PinglessIdleHandlerFactory());
    }
}
