package com.bet.blues.proxy.socket.multi;

import java.util.regex.*;

/**
 * User: mihai.vass
 * Date: 3/12/13
 */
public class SubjectRoutingData {

    private String subject;
    private String socketId;
    private Pattern subjectPattern;

    public SubjectRoutingData(String subject, String socketId) {
        this.subject = subject;
        this.socketId = socketId;
        subjectPattern = Pattern.compile(subject);
    }

    public String getSubject() {
        return subject;
    }

    public String getSocketId() {
        return socketId;
    }

    public Pattern getSubjectPattern() {
        return subjectPattern;
    }

    @Override
    public String toString() {
        return subject + "~" + socketId;
    }
}
