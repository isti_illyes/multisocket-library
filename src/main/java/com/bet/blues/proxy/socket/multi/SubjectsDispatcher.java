package com.bet.blues.proxy.socket.multi;

import java.util.*;
import java.util.regex.*;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * User: mihai.vass
 * Date: 3/11/13
 */
public class SubjectsDispatcher<T> {

    private static final Logger LOGGER = LogManager.getLogger(SubjectsDispatcher.class);

    private SubjectsRouting subjectsRouting = new SubjectsRouting();
    private Map<String, T> processorsBySocketId = new HashMap<String, T>();
    private Map<String, T> processorsBySubject = new HashMap<String, T>();

    public synchronized void init(String defaultRoute, List<SubjectRoutingData> routingTable) {
        subjectsRouting.setDefaultRoute(defaultRoute);
        subjectsRouting.clearRoutingInfo();
        subjectsRouting.addRoutingInfo(routingTable);
        clearProcessors();
    }

    public void setSubjectsRouting(SubjectsRouting subjectsRouting) {
        this.subjectsRouting = subjectsRouting;
    }

    public int getNumberOfDedicatedConnections() {
        final LinkedHashMap<String, List<SubjectRoutingData>> routingTableBySocket = subjectsRouting.collapseRoutingTableBySocket();
        routingTableBySocket.remove(subjectsRouting.getDefaultRoute());
        return routingTableBySocket.size();
    }

    public synchronized void linkSocketToProcessor(String socketId, T processor) {
        processorsBySocketId.put(socketId, processor);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("multi.socket link socket to processor: " + socketId);
        }
    }

    public synchronized void linkSubjectToProcessor(String subject, T processor) {
        processorsBySubject.put(subject, processor);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("multi.socket link subject to processor: " + subject);
        }
    }

    public synchronized T getProcessor(String subject) {
        // check if subject exists
        T processor = processorsBySubject.get(subject);
        if (processor == null) {
            // check a matching subject pattern
            for (SubjectRoutingData routingData : subjectsRouting.getRoutingTable()) {
                final Pattern subjectPattern = routingData.getSubjectPattern();
                if (subjectPattern.matcher(subject).matches()) {
                    processor = processorsBySocketId.get(routingData.getSocketId());

                    // add subject => expand subject pattern
                    linkSubjectToProcessor(subject, processor);
                    break;
                }
            }
        }

        return processor;
    }

    public synchronized void clearProcessors() {
        processorsBySocketId.clear();
        processorsBySubject.clear();
    }
}
