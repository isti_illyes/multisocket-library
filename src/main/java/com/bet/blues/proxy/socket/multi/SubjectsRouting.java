package com.bet.blues.proxy.socket.multi;

import java.util.*;

import com.bet.blues.proxy.socket.multi.SubjectRoutingData;
import com.bet.blues.util.Static;

/**
 * User: mihai.vass
 * Date: 3/12/13
 */
public class SubjectsRouting {

    public static final String VALUE_SEPARATOR = ":";
    public static final String ITEM_SEPARATOR = ",";

    private String defaultRoute;
    private List<SubjectRoutingData> routingTable = new ArrayList<SubjectRoutingData>();

    public String getDefaultRoute() {
        return defaultRoute;
    }

    public void setDefaultRoute(String defaultRoute) {
        this.defaultRoute = defaultRoute;
    }

    public List<SubjectRoutingData> getRoutingTable() {
        return routingTable;
    }

    public void addRoutingInfo(Collection<SubjectRoutingData> routingTable) {
        this.routingTable.addAll(routingTable);
    }

    public void clearRoutingInfo() {
        this.routingTable.clear();
    }

    public String packRoutingTable() {
        return Static.join(routingTable, new Static.Transformer<SubjectRoutingData, String>() {
            @Override
            public String transform(SubjectRoutingData item) {
                return item.getSubject() + VALUE_SEPARATOR + item.getSocketId();
            }
        }, ITEM_SEPARATOR);
    }

    public static List<SubjectRoutingData> unpackRoutingTable(String data) {
        final List<SubjectRoutingData> result = new ArrayList<SubjectRoutingData>();
        for (String tableEntry : Static.split(data, ITEM_SEPARATOR)) {
            final String[] tokens = tableEntry.split(VALUE_SEPARATOR);
            if (tokens.length == 2) {
                final String subject = tokens[0].trim();
                final String socketID = tokens[1].trim();
                result.add(new SubjectRoutingData(subject, socketID));
            }
        }

        return result;
    }

    public LinkedHashMap<String, List<SubjectRoutingData>> collapseRoutingTableBySocket() {
        final LinkedHashMap<String, List<SubjectRoutingData>> result = new LinkedHashMap<String, List<SubjectRoutingData>>();
        for (SubjectRoutingData routingData : routingTable) {
            List<SubjectRoutingData> routingDataBySocket = result.get(routingData.getSocketId());
            if (routingDataBySocket == null) {
                routingDataBySocket = new ArrayList<SubjectRoutingData>();
                result.put(routingData.getSocketId(), routingDataBySocket);
            }

            routingDataBySocket.add(routingData);
        }
        return result;
    }
}
