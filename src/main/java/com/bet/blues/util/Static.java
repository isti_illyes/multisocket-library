package com.bet.blues.util;

import java.util.*;

/**
 * A unique ID associated with Objects
 * The following routines are comic from public license:
 * split
 *
 * @author Gaspar Sinai <gsinai@blue-edge-tech.com>
 * @version 2000-03-02
 */
public final class Static {

    private Static() {
    }

    public static List<String> splitToList(String str, String delimiter) {
        int l = str.length(), i = 0;
        List<String> result = new ArrayList<String>();
        while (i < l) {
            int p = str.indexOf(delimiter, i);
            if (-1 == p) {
                result.add(str.substring(i));
                break;
            } else {
                result.add(str.substring(i, p));
                i = p + delimiter.length();
            }
        }
        return result;
    }

    public static String[] split(String str, String delimiter) {
        List<String> result = splitToList(str, delimiter);
        return result.toArray(new String[result.size()]);
    }


    public static <T, V> String join(Collection<T> values, Transformer<T, V> transformer, String sep) {
        return join("", values, transformer, sep);
    }

    public static <T, V> String join(String header, Collection<T> values, Transformer<T, V> transformer, String sep) {
        if (values == null || transformer == null) {
            return "";
        }

        Iterator<T> it = values.iterator();
        StringBuilder sb = new StringBuilder(header);
        if (it.hasNext()) {
            sb.append(transformer.transform(it.next()));
            while (it.hasNext()) {
                sb.append(sep).append(transformer.transform(it.next()));
            }
        }

        return sb.toString();
    }

    public interface Transformer<T, V> {
        V transform(T item);
    }
}
