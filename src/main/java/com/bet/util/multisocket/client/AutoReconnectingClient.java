package com.bet.util.multisocket.client;

import com.bet.util.multisocket.common.MessageDispatcher;
import com.bet.util.multisocket.common.Socket;
import com.bet.util.multisocket.common.endpoint.AutoReconnectingEndpoint;
import com.bet.util.multisocket.common.endpoint.EndpointListener;
import com.bet.util.multisocket.common.handler.AbstractIdleHandlerFactory;
import com.bet.blues.proxy.socket.multi.SubjectsDispatcher;

/**
 * Created by isti on 1/12/14.
 */
public class AutoReconnectingClient extends AbstractClient {

    private final EndpointListener<AutoReconnectingClientEndpoint> endpointListener;
    private AbstractIdleHandlerFactory idleHandlerFactory;

    protected AutoReconnectingClient(
            ClientContext context,
            EndpointListener<AutoReconnectingClientEndpoint> endpointListener,
            AbstractIdleHandlerFactory idleHandlerFactory
    ) {
        super(context);
        this.endpointListener = endpointListener;
        this.idleHandlerFactory = idleHandlerFactory;
    }

    @Override
    protected ClientEventsProcessor createClientEventsProcessor(
            final String clientId,
            final SubjectsDispatcher<Socket> subjectsDispatcher,
            final ReconnectionManager reconnectionManager
    ) {
        final MessageDispatcher messageDispatcher = new MessageDispatcher(subjectsDispatcher);
        final AutoReconnectingEndpoint autoReconnectingEndpoint =
                new AutoReconnectingEndpoint(clientId, messageDispatcher, endpointListener, reconnectionManager);
        autoReconnectingEndpoint.initIdleHandler(
                idleHandlerFactory, getContext().getTimeoutMillis(), getContext().getClientId()
        );

        return new ClientEventsProcessor(
                subjectsDispatcher, getClientSocketManager(), autoReconnectingEndpoint
        );
    }
}
