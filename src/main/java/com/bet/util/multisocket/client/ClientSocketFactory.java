package com.bet.util.multisocket.client;

import java.net.*;

import com.bet.util.multisocket.common.BootstrapFactory;
import com.bet.util.multisocket.common.ChannelType;
import com.bet.util.multisocket.common.Resources;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import org.apache.log4j.Logger;

/**
 * User: istvan.illyes
 * Date: 1/20/14
 */
final class ClientSocketFactory {

    private static final Logger LOGGER = Logger.getLogger(ClientSocketFactory.class);

    /**
     * Creates a socket to connectOnMasterSocket to server on port and ip address of the current instance
     *
     * @return The {@link com.bet.util.multisocket.common.Socket}
     */
    protected static com.bet.util.multisocket.common.Socket createConnectorSocket(
            ChannelType channelType,
            final ClientHandler clientHandler,
            InetSocketAddress remoteAddress
    ) {
        final Bootstrap bootstrap = BootstrapFactory.createBootstrap(channelType);

        bootstrap.handler(new ClientChannelInitializer(clientHandler));

        try {
            final ChannelFuture channelFuture = bootstrap.connect(new InetSocketAddress(remoteAddress.getAddress(), remoteAddress.getPort()))
                    .sync();
            channelFuture.awaitUninterruptibly();
            if (channelFuture.isSuccess()) {
                return new com.bet.util.multisocket.common.Socket(channelFuture.channel());

            } else {
                LOGGER.warn(String.format(
                        "Failed to open socket! Cannot connect to ip: %s port: %d!",
                        remoteAddress.getAddress(), remoteAddress.getPort())
                );
            }
        } catch (InterruptedException e) {
            LOGGER.error("Failed to open socket!", e);
        }
        return null;
    }

    private static class ClientChannelInitializer extends ChannelInitializer<SocketChannel> {

        private ChannelHandler clientHandler;

        private ClientChannelInitializer(ChannelHandler clientHandler) {
            this.clientHandler = clientHandler;
        }

        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
            // Encoders
            ch.pipeline().addLast(Resources.STRING_ENCODER_NAME, Resources.STRING_ENCODER);

            // Decoders
            ch.pipeline().addLast(Resources.DELIMITER_DECODER_NAME,
                    new DelimiterBasedFrameDecoder(Integer.MAX_VALUE, Delimiters.lineDelimiter()));
            ch.pipeline().addLast(Resources.STRING_DECODER_NAME, Resources.STRING_DECODER);

            // Handlers
            ch.pipeline().addLast(Resources.LOGGING_HANDLER_NAME, Resources.LOGGING_HANDLER);
            ch.pipeline().addLast(Resources.CLIENT_HANDLER_NAME, clientHandler);
        }
    }
}
