package com.bet.util.multisocket.client;

import java.net.*;
import java.util.*;

import com.bet.util.multisocket.common.ChannelType;
import com.bet.util.multisocket.common.InvalidStateException;
import com.bet.util.multisocket.common.Socket;
import com.bet.blues.proxy.socket.multi.SubjectsRouting;
import com.bet.util.multisocket.protocol.MessageFactory;

/**
 * User: istvan.illyes
 * Date: 8/13/13
 */
public class ClientSocketManager {

    private final String clientId;
    private final ChannelType channelType;
    private ClientHandler clientHandler;
    private InetSocketAddress remoteAddress;
    private boolean started = false;

    private ClientSocketManager(
            final String clientId,
            final ChannelType channelType,
            final InetSocketAddress remoteAddress
    ) {
        this.clientId = clientId;
        this.channelType = channelType;
        this.remoteAddress = remoteAddress;
    }

    public static ClientSocketManager create(final ClientContext context) {
        return new ClientSocketManager(context.getClientId(), context.getChannelType(), context.getRemoteAddress());
    }

    protected void start(ClientHandler clientHandler) throws InvalidStateException {
        if (started) {
            throw new InvalidStateException(String.format("Failed to start %s. Already started",
                    this.getClass().getName()));
        }
        started = true;
        this.clientHandler = clientHandler;
    }

    protected void stop() {
        started = false;
        this.clientHandler = null;
    }

    /**
     * Creates the master socket and connects to server.
     * Sends a message on the master socket, to inform the server about the new connection.
     *
     * @param clientId
     * @param timeoutMillis
     * @param reconnect
     */
    protected Socket createMasterSocket(
            final String clientId, final int timeoutMillis, final boolean reconnect
    ) throws InvalidStateException {
        if (!started) {
            throw new InvalidStateException(String.format("Failed to create master socket. %s started : %b",
                    this.getClass().getName(), started));
        }
        final String connectMessage;
        if (reconnect) {
            connectMessage = MessageFactory.MasterSocketReconnect.buildMessage(clientId, timeoutMillis);
        } else {
            connectMessage = MessageFactory.MasterSocketConnect.buildMessage(clientId, timeoutMillis);
        }
        Socket masterSocket = ClientSocketFactory.createConnectorSocket(channelType, clientHandler, remoteAddress);
        masterSocket.writeAndFlush(connectMessage);
        return masterSocket;
    }

    /**
     * Creates the custom sockets and links them to their socketIds
     *
     * @param subjectsRouting Object containing the routing table
     */
    protected Map<String, Socket> createCustomSockets(final SubjectsRouting subjectsRouting) throws InvalidStateException {
        if (started) {
            final Map<String, Socket> socketsById = new HashMap<String, Socket>();
            for (String socketId : subjectsRouting.collapseRoutingTableBySocket().keySet()) {
                if (!socketId.equals(subjectsRouting.getDefaultRoute())) {
                    final Socket socket = ClientSocketFactory.createConnectorSocket(channelType, clientHandler, remoteAddress);

                    socketsById.put(socketId, socket);
                    socket.writeAndFlush(MessageFactory.CustomSocketConnect.buildMessage(socketId, clientId));
                }
            }
            return socketsById;
        }
        throw new InvalidStateException(String.format("Failed to create master socket. %s started : %b",
                this.getClass().getName(), started));
    }

    public void setRemoteAddress(InetSocketAddress remoteAddress) {
        this.remoteAddress = remoteAddress;
    }
}
