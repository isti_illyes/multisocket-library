package com.bet.util.multisocket.client;

import com.bet.util.multisocket.common.handler.IdleHandlerFactory;

/**
 * User: istvan.illyes
 * Date: 2/5/14
 */
public final class ClientFactory extends AbstractClientFactory {

    public ClientFactory() {
        super(new IdleHandlerFactory());
    }
}
