package com.bet.util.multisocket.client;

import com.bet.util.multisocket.common.MessageDispatcher;
import com.bet.util.multisocket.common.Resources;
import com.bet.util.multisocket.common.Socket;
import com.bet.util.multisocket.common.endpoint.EndpointListener;
import com.bet.util.multisocket.common.endpoint.ManualReconnectingEndpoint;
import com.bet.util.multisocket.common.handler.AbstractIdleHandlerFactory;
import com.bet.blues.proxy.socket.multi.SubjectsDispatcher;

/**
 * Created by isti on 1/12/14.
 */
public class ManualReconnectingClient extends AbstractClient {

    private EndpointListener<ManualReconnectingClientEndpoint> endpointListener;
    private AbstractIdleHandlerFactory idleHandlerFactory;

    protected ManualReconnectingClient(
            ClientContext context,
            EndpointListener<ManualReconnectingClientEndpoint> endpointListener,
            AbstractIdleHandlerFactory idleHandlerFactory
    ) {
        super(context);
        this.endpointListener = endpointListener;
        this.idleHandlerFactory = idleHandlerFactory;
    }

    @Override
    protected ClientEventsProcessor createClientEventsProcessor(
            final String clientId,
            final SubjectsDispatcher<Socket> subjectsDispatcher,
            final ReconnectionManager reconnectionManager
    ) {
        final MessageDispatcher messageDispatcher = new MessageDispatcher(subjectsDispatcher);
        final ManualReconnectingEndpoint manualReconnectingEndpoint =
                new ManualReconnectingEndpoint(
                        clientId, messageDispatcher, endpointListener,
                        reconnectionManager, Resources.MANUAL_RECONNECT_TIMEOUT_MILLIS
                );
        manualReconnectingEndpoint.initIdleHandler(
                idleHandlerFactory, getContext().getTimeoutMillis(), getContext().getClientId()
        );

        return new ClientEventsProcessor(
                subjectsDispatcher, getClientSocketManager(), manualReconnectingEndpoint
        );
    }
}
