package com.bet.util.multisocket.client;

import com.bet.util.multisocket.common.endpoint.EndpointListener;
import com.bet.util.multisocket.common.handler.AbstractIdleHandlerFactory;

/**
 * User: istvan.illyes
 * Date: 2/5/14
 */
public abstract class AbstractClientFactory {

    private final AbstractIdleHandlerFactory idleHandlerFactory;

    public AbstractClientFactory(AbstractIdleHandlerFactory idleHandlerFactory) {
        this.idleHandlerFactory = idleHandlerFactory;
    }

    public AutoReconnectingClient createAutoReconnectingClient(
            ClientContext context,
            EndpointListener<AutoReconnectingClientEndpoint> endpointListener
    ) {
        return new AutoReconnectingClient(context, endpointListener, idleHandlerFactory);
    }

    public ManualReconnectingClient createManualReconnectingClient(
            ClientContext context,
            EndpointListener<ManualReconnectingClientEndpoint> endpointListener
    ) {
        return new ManualReconnectingClient(context, endpointListener, idleHandlerFactory);
    }
}
