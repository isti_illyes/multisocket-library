package com.bet.util.multisocket.client;

import com.bet.util.multisocket.common.InvalidStateException;
import com.bet.util.multisocket.common.Socket;
import com.bet.blues.proxy.socket.multi.SubjectsDispatcher;

/**
 * User: istvan.illyes
 * Date: 7/1/13
 */
public abstract class AbstractClient {

    private ClientContext context;
    private ClientSocketManager clientSocketManager;
    private volatile boolean started = false;

    protected AbstractClient(ClientContext context) {
        this.context = context;
        this.clientSocketManager = ClientSocketManager.create(context);
    }

    /**
     * Starts the client
     */
    public void start() throws InvalidStateException {
        if (started) {
            throw new InvalidStateException(String.format("Failed to start %s. Already started",
                    this.getClass().getName()));
        }
        started = true;
        final ReconnectionManager reconnectionManager = new ReconnectionManager(context, clientSocketManager);
        final SubjectsDispatcher<Socket> subjectsDispatcher = new SubjectsDispatcher<Socket>();

        // create manual reconnecting endpoint
        final ClientEventsProcessor clientEventsProcessor =
                createClientEventsProcessor(context.getClientId(), subjectsDispatcher, reconnectionManager);
        final ClientHandler clientHandler = new ClientHandler(clientEventsProcessor);
        reconnectionManager.setReconnectObserver(clientHandler);

        // start socket manager and create master socket
        clientSocketManager.start(clientHandler);
        clientSocketManager.createMasterSocket(context.getClientId(), context.getTimeoutMillis(), false);
    }

    /**
     * Stops the client
     */
    public void stop() {
        started = false;
        clientSocketManager.stop();
    }

    protected abstract ClientEventsProcessor createClientEventsProcessor(
            final String clientId,
            final SubjectsDispatcher<Socket> subjectsDispatcher,
            final ReconnectionManager reconnectionManager
    );

    protected ClientContext getContext() {
        return context;
    }

    protected ClientSocketManager getClientSocketManager() {
        return clientSocketManager;
    }
}
