package com.bet.util.multisocket.client;

import java.net.*;

import com.bet.util.multisocket.common.ChannelType;


/**
 * User: istvan.illyes
 * Date: 7/9/13
 * <p/>
 * This class is used to configure a client session
 */
public final class ClientContext {

    private final int timeoutMillis;
    private final String clientId;
    private final ChannelType channelType;
    private final InetSocketAddress remoteAddress;

    private ClientContext(final ClientContextBuilder builder) {
        this.remoteAddress = builder.remoteAddress;
        this.timeoutMillis = builder.timeoutMillis;
        this.clientId = builder.clientId;
        this.channelType = builder.channelType;
    }

    public static ClientContextBuilder createBuilder() {
        return new ClientContextBuilder();
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public InetSocketAddress getRemoteAddress() {
        return remoteAddress;
    }

    public int getTimeoutMillis() {
        return timeoutMillis;
    }

    public String getClientId() {
        return clientId;
    }

    public static final class ClientContextBuilder<T> {

        private InetSocketAddress remoteAddress;
        private int timeoutMillis;
        private String clientId;
        private ChannelType channelType;

        public ClientContextBuilder remoteAddress(final InetSocketAddress remoteAddress) {
            this.remoteAddress = remoteAddress;
            return this;
        }

        public ClientContextBuilder timeoutMillis(final int timeoutMillis) {
            this.timeoutMillis = timeoutMillis;
            return this;
        }

        public ClientContextBuilder clientId(final String clientId) {
            this.clientId = clientId;
            return this;
        }

        public ClientContextBuilder channelType(final ChannelType channelType) {
            this.channelType = channelType;
            return this;
        }

        public ClientContext build() {
            return new ClientContext(this);
        }
    }
}
