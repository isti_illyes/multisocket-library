package com.bet.util.multisocket.client;

import java.util.*;

import com.bet.blues.proxy.socket.multi.SubjectsRouting;
import com.bet.util.multisocket.common.ReconnectObserver;
import com.bet.util.multisocket.common.Socket;
import com.bet.blues.proxy.socket.multi.SubjectRoutingData;
import com.bet.util.multisocket.protocol.MessageFactory;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.apache.log4j.Logger;


/**
 * User: istvan.illyes
 * Date: 7/1/13
 * <p/>
 * Handler class responsible of handling different events that happen on a socket.
 * Events like message receiving and dispatching or notifications when a connection was closed.
 */
@ChannelHandler.Sharable
public final class ClientHandler extends ChannelInboundHandlerAdapter implements ReconnectObserver {

    private static final Logger LOGGER = Logger.getLogger(ClientHandler.class);
    private ClientEventsProcessor clientEventsProcessor;
    private volatile boolean connected = false;

    public ClientHandler(ClientEventsProcessor clientEventsProcessor) {
        this.clientEventsProcessor = clientEventsProcessor;
    }

    /**
     * Called every time a new message is received on a channel.
     * Takes care of message processing and replying.
     *
     * @param ctx
     * @param msg
     * @throws Exception
     */
    @Override
    public void channelRead(final ChannelHandlerContext ctx, final Object msg) throws Exception {
        final Socket socket = new Socket(ctx.channel());
        final String message = ((String) msg);

        if (connected && !MessageFactory.isPingMessage(message)) {
                clientEventsProcessor.processCommonMessage(message);
        } else {
            // received: master socket [socket id + routing table]
            if (MessageFactory.SubjectsRouting.isRoutingTableMessage(message)) {
                final SubjectsRouting subjectsRouting = MessageFactory.SubjectsRouting.parseMessage(message);
                final String socketId = subjectsRouting.getDefaultRoute();
                if (socketId == null) {
                    LOGGER.error("Invalid master socketId message, closing socket...");
                    socket.close();
                    return;
                }

                final List<SubjectRoutingData> routingTable = subjectsRouting.getRoutingTable();
                if (routingTable == null) {
                    LOGGER.error("Invalid routing table message, closing socket...");
                    socket.close();
                    return;
                }

                try {
                    // open custom sockets, for each send [socket id, client id]
                    clientEventsProcessor.processRoutingTableMessage(socketId, routingTable, socket);
                } catch (IllegalStateException ex) {
                    forceClose(socket, ex);
                }

                // received: custom socket [ACK message - all connections established]
            } else if (MessageFactory.AllConnectionsEstablished.isAllConnectionsEstablishedMessage(message)) {
                try {
                    clientEventsProcessor.processAllConnectionsEstablishedEvent();
                    connected = true;
                } catch (IllegalStateException ex) {
                    forceClose(socket, ex);
                }

                // received: master socket [ACK message - reconnect]
            } else if (MessageFactory.MasterSocketReconnectAck.isReconnectionAckMessage(message)) {
                try {
                    clientEventsProcessor.processReconnectionAckMessage(socket);
                } catch (IllegalStateException ex) {
                    forceClose(socket, ex);
                }

                // common message
            } else if (!MessageFactory.isPingMessage(message)) {
                clientEventsProcessor.processCommonMessage(message);
            }
        }
    }

    /**
     * Called every time a channel is closed.
     * Notifies the processor that a channel was closed.
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(final ChannelHandlerContext ctx) throws Exception {
        clientEventsProcessor.processSocketClosedEvent(new Socket(ctx.channel()));
    }

    /**
     * Called every time an exception is raised in the handlers.
     *
     * @param ctx
     * @param cause
     * @throws java.lang.Exception
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        LOGGER.error(String.format("Exception in %s", this.getClass().getName()), cause);
    }

    /**
     * Closes the socket and logs error message.
     *
     * @param socket
     * @param ex
     */
    private void forceClose(final Socket socket, final IllegalStateException ex) {
        LOGGER.error("Illegal client state, closing socket...", ex);
        socket.close();
    }

    @Override
    public void notifyReconnect() {
        connected = false;
    }
}
