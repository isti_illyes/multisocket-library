package com.bet.util.multisocket.client;

import java.net.*;

import com.bet.util.multisocket.common.InvalidStateException;
import com.bet.util.multisocket.common.Reconnectable;
import com.bet.util.multisocket.common.endpoint.MultiSocketEndpoint;
import com.bet.util.multisocket.common.endpoint.ManualReconnectingEndpoint;

/**
 * User: istvan.illyes
 * Date: 1/29/14
 */
public final class ManualReconnectingClientEndpoint extends MultiSocketEndpoint implements Reconnectable {

    private ManualReconnectingEndpoint endpoint;

    public ManualReconnectingClientEndpoint(ManualReconnectingEndpoint endpoint) {
        this.endpoint = endpoint;
    }

    @Override
    public void sendMessage(String message, String subject) throws InvalidStateException {
        endpoint.sendMessage(message, subject);
    }

    @Override
    public String readMessage() throws InterruptedException {
        return endpoint.readMessage();
    }

    @Override
    public void close() {
        endpoint.close();
    }

    @Override
    public void reconnect() throws InvalidStateException {
        endpoint.reconnect();
    }

    @Override
    public SocketAddress getLocalAddress() throws InvalidStateException {
        return endpoint.getLocalAddress();
    }

    @Override
    public SocketAddress getRemoteAddress() throws InvalidStateException {
        return endpoint.getRemoteAddress();
    }
}
