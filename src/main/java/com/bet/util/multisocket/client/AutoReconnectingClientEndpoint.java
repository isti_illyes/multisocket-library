package com.bet.util.multisocket.client;

import java.net.*;

import com.bet.util.multisocket.common.InvalidStateException;
import com.bet.util.multisocket.common.endpoint.MultiSocketEndpoint;
import com.bet.util.multisocket.common.endpoint.AutoReconnectingEndpoint;

/**
 * User: istvan.illyes
 * Date: 1/29/14
 */
public final class AutoReconnectingClientEndpoint extends MultiSocketEndpoint {

    private AutoReconnectingEndpoint endpoint;

    public AutoReconnectingClientEndpoint(AutoReconnectingEndpoint endpoint) {
        this.endpoint = endpoint;
    }

    @Override
    public void sendMessage(String message, String subject) throws InvalidStateException {
        endpoint.sendMessage(message, subject);
    }

    @Override
    public String readMessage() throws InterruptedException {
        return endpoint.readMessage();
    }

    @Override
    public void close() {
        endpoint.close();
    }

    @Override
    public SocketAddress getLocalAddress() throws InvalidStateException {
        return endpoint.getLocalAddress();
    }

    @Override
    public SocketAddress getRemoteAddress() throws InvalidStateException {
        return endpoint.getRemoteAddress();
    }
}
