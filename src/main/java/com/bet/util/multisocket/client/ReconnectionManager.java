package com.bet.util.multisocket.client;

import java.util.concurrent.*;

import com.bet.util.multisocket.common.InvalidStateException;
import com.bet.util.multisocket.common.ReconnectObserver;
import com.bet.util.multisocket.common.Resources;
import org.apache.log4j.Logger;

/**
 * User: istvan.illyes
 * Date: 8/13/13
 */
public class ReconnectionManager {

    private static final Logger LOGGER = Logger.getLogger(ReconnectionManager.class);
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();
    private final ClientSocketManager clientSocketManager;
    private final ClientContext context;
    private final Object reconnectionLock = new Object();
    private ReconnectObserver reconnectObserver;
    private volatile boolean reconnecting = false;


    public ReconnectionManager(ClientContext context, ClientSocketManager clientSocketManager) {
        this.context = context;
        this.clientSocketManager = clientSocketManager;
    }

    /**
     * Tries to reconnect to server. If reconnection fails it retries. The number of retries
     * is given as parameter.
     *
     * @param noRetries the number of retries
     * @return true if reconnection succeeded
     *         false otherwise
     */
    public synchronized boolean tryReconnect(int noRetries) throws InvalidStateException {
        for (int i = 0; i < noRetries; i++) {
            if (!reconnecting) {
                reconnecting = true;
                executeReconnection();
            }
            final Future<?> reconnectFuture = createWaitForReconnectFuture();
            if (reconnectFuture != null) {
                try {
                    reconnectFuture.get(Resources.RECONNECT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
                    return true;
                } catch (Exception e) {
                    LOGGER.warn("Reconnection failed!", e);
                }
            }
            reconnecting = false;
        }
        return false;
    }

    /**
     * Reconnects to server
     */
    private synchronized Future<?> createWaitForReconnectFuture() {
        return EXECUTOR_SERVICE.submit(createWaitForReconnectionRunnable());
    }

    private Runnable createWaitForReconnectionRunnable() {
        return new Runnable() {
            @Override
            public void run() {
                // Waits until reconnection is completed
                synchronized (reconnectionLock) {
                    while (reconnecting) {
                        try {
                            // Makes the current thread wait until reconnectionLock is notified
                            reconnectionLock.wait();
                        } catch (InterruptedException e) {
                            LOGGER.warn("Exception caught while reconnecting" + e);
                        }
                    }
                }
            }
        };
    }

    /**
     * Opens the master socket and connects to server
     */
    private void executeReconnection() throws InvalidStateException {
        if (reconnectObserver != null) {
            reconnectObserver.notifyReconnect();
        }
        clientSocketManager.createMasterSocket(context.getClientId(), context.getTimeoutMillis(), true);
    }

    public void notifyConnectionEstablished() {
        synchronized (reconnectionLock) {
            this.reconnecting = false;
            reconnectionLock.notifyAll();
        }
    }

    public void setReconnectObserver(ClientHandler reconnectObserver) {
        this.reconnectObserver = reconnectObserver;
    }
}
