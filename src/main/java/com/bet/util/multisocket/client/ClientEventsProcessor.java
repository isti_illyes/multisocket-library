package com.bet.util.multisocket.client;

import java.util.*;
import java.util.concurrent.locks.*;

import com.bet.util.multisocket.common.InvalidStateException;
import com.bet.util.multisocket.common.Socket;
import com.bet.util.multisocket.common.endpoint.Endpoint;
import com.bet.blues.proxy.socket.multi.SubjectRoutingData;
import com.bet.blues.proxy.socket.multi.SubjectsDispatcher;
import com.bet.blues.proxy.socket.multi.SubjectsRouting;

/**
 * User: istvan.illyes
 * Date: 7/16/13
 * <p/>
 * This class takes care of processing received messages.
 */
class ClientEventsProcessor {

    private final SubjectsDispatcher<Socket> subjectsDispatcher;
    private final ClientSocketManager clientSocketManager;
    private final Endpoint endpoint;
    private final Lock closeLock = new ReentrantLock();
    private SubjectsRouting subjectsRouting;

    protected ClientEventsProcessor(
            final SubjectsDispatcher<Socket> subjectsDispatcher,
            final ClientSocketManager clientSocketManager,
            final Endpoint endpoint

    ) {
        this.subjectsDispatcher = subjectsDispatcher;
        this.clientSocketManager = clientSocketManager;
        this.endpoint = endpoint;
    }

    /**
     * Adds the routing data to {@link SubjectsRouting}.
     * Links the master socket to defaultRoute.
     * Opens the custom sockets.
     *
     * @param defaultRoute
     * @param routingTable
     * @param masterSocket
     * @throws InvalidStateException
     */
    public void processRoutingTableMessage(
            final String defaultRoute,
            final List<SubjectRoutingData> routingTable,
            final Socket masterSocket
    ) throws InvalidStateException {
        subjectsRouting = new SubjectsRouting();
        subjectsRouting.setDefaultRoute(defaultRoute);
        if (routingTable != null) {
            subjectsRouting.addRoutingInfo(routingTable);
            subjectsDispatcher.init(subjectsRouting.getDefaultRoute(), subjectsRouting.getRoutingTable());
            linkSocketIdsToSockets(masterSocket);

        } else {
            endpoint.connectionFailed();
            throw new InvalidStateException("Routing table is empty!");
        }
    }

    /**
     * Links the MASTER socket to defaultRoute.
     * Opens the custom sockets.
     *
     * @param masterSocket
     * @throws InvalidStateException
     */
    public void processReconnectionAckMessage(final Socket masterSocket) throws InvalidStateException {
        linkSocketIdsToSockets(masterSocket);
    }

    /**
     * Notifies the endpoint and the client listener
     */
    public void processAllConnectionsEstablishedEvent() throws InvalidStateException {
        endpoint.notifyConnectionEstablished();
        if (!endpoint.isEstablished()) {
            endpoint.connectionEstablished();
        } else {
            endpoint.reconnectionSuccessful();
        }
    }

    /**
     * Called when a socket was closed.
     * Executes a task on a different thread, which disconnects the endpoint and notifies the endpoint listener.
     *
     * @param socket
     */
    public void processSocketClosedEvent(final Socket socket) {
        closeLock.lock();
        try {
            if (endpoint.containsSocket(socket)) {
                endpoint.disconnect();
                endpoint.connectionClosed();
            }
        } finally {
            closeLock.unlock();
        }
    }

    /**
     * Notifies the endpoint about the received message.
     *
     * @param message
     */
    public void processCommonMessage(String message) {
        endpoint.notifyMessageReceived(message);
    }

    private void linkSocketIdsToSockets(Socket masterSocket) throws InvalidStateException {
        if (subjectsRouting != null) {
            Map<String, Socket> socketsById = new HashMap<String, Socket>();
            socketsById.put(subjectsRouting.getDefaultRoute(), masterSocket);
            socketsById.putAll(clientSocketManager.createCustomSockets(subjectsRouting));
            for (Map.Entry<String, Socket> entry : socketsById.entrySet()) {
                entry.getValue().setSocketId(entry.getKey());
                subjectsDispatcher.linkSocketToProcessor(entry.getKey(), entry.getValue());
                endpoint.addSocket(entry.getValue());
            }
            endpoint.setMasterSocket(masterSocket);

        } else {
            throw new InvalidStateException(
                    String.format("Failed to link socketsIds to sockets. Subjects routing: %s", subjectsRouting));
        }
    }
}
