package com.bet.util.multisocket.server;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import java.util.concurrent.locks.*;

import com.bet.util.multisocket.common.InvalidStateException;
import com.bet.util.multisocket.common.MessageDispatcher;
import com.bet.util.multisocket.common.Resources;
import com.bet.util.multisocket.common.Socket;
import com.bet.util.multisocket.common.endpoint.EndpointListener;
import com.bet.util.multisocket.common.endpoint.SimpleServerEndpoint;
import com.bet.util.multisocket.common.handler.AbstractIdleHandlerFactory;
import com.bet.blues.proxy.socket.multi.SubjectsDispatcher;
import org.apache.log4j.Logger;

/**
 * User: istvan.illyes
 * Date: 7/15/13
 * <p/>
 * This class takes care of processing received messages.
 */
class ServerEventsProcessor {

    private static final Logger LOGGER = Logger.getLogger(ServerEventsProcessor.class);
    private final ServerContext context;
    private final AbstractIdleHandlerFactory idleHandlerFactory;
    private final EndpointListener<ServerEndpoint> endpointListener;
    private final Map<String, SimpleServerEndpoint> endpointByClientId =
            new ConcurrentHashMap<String, SimpleServerEndpoint>();
    private final Map<Socket, SimpleServerEndpoint> endpointBySocket =
            new ConcurrentHashMap<Socket, SimpleServerEndpoint>();
    private final Map<SimpleServerEndpoint, Long> lastCloseTimeByEndpoint =
            new ConcurrentHashMap<SimpleServerEndpoint, Long>();
    private final Map<SimpleServerEndpoint, AtomicInteger> establishedConnectionsByEndpoint =
            new ConcurrentHashMap<SimpleServerEndpoint, AtomicInteger>();
    private final Map<SimpleServerEndpoint, SubjectsDispatcher<Socket>> subjectsDispatcherByEndpoint =
            new ConcurrentHashMap<SimpleServerEndpoint, SubjectsDispatcher<Socket>>();
    private final Lock closeLock = new ReentrantLock();

    public ServerEventsProcessor(
            final ServerContext context,
            final AbstractIdleHandlerFactory idleHandlerFactory,
            final EndpointListener<ServerEndpoint> endpointListener
            ) {
        this.context = context;
        this.idleHandlerFactory = idleHandlerFactory;
        this.endpointListener = endpointListener;
    }

    /**
     * Processes a connect message on master socket.
     * Creates an endpoint and a connection counter.
     *
     * @param masterSocket
     * @param clientId
     * @param timeout
     * @return true if message was successfully processed
     *         false if failed to process message
     */
    public void processMasterSocketConnectedMessage(
            final Socket masterSocket, final String clientId, final int timeout) throws InvalidStateException {
        SimpleServerEndpoint endpoint = createMultiSocketEndpoint(clientId, masterSocket, timeout);

        // store endpoint
        endpointByClientId.put(clientId, endpoint);

        // create connection counter for client
        final AtomicInteger establishedConnections = new AtomicInteger(0);
        establishedConnectionsByEndpoint.put(endpoint, establishedConnections);

        processConnection(endpoint, context.getSubjectsRouting().getDefaultRoute(), masterSocket);
    }

    /**
     * Processes a reconnect message on master socket.
     * Cleans up the old endpoint and processes the new connection.
     *
     * @param masterSocket
     * @param timeout
     */
    public void processMasterSocketReconnectedMessage(
            final Socket masterSocket, final String clientId, final int timeout
    ) throws InvalidStateException {
        final SimpleServerEndpoint endpoint = endpointByClientId.get(clientId);

        if (endpoint == null) {
            throw new InvalidStateException(
                    "Failed to process reconnect. Endpoint not found for client: "
                            + clientId);
        }

        processClose(endpoint);
        endpoint.initIdleHandler(idleHandlerFactory, timeout, clientId);
        endpoint.setMasterSocket(masterSocket);
        processConnection(endpoint, context.getSubjectsRouting().getDefaultRoute(), masterSocket);
    }

    public synchronized ConnectionsStatus processCustomSocketConnectedMessage(
            final Socket socket, final String socketId, final String clientId) throws InvalidStateException {
        final SimpleServerEndpoint endpoint = endpointByClientId.get(clientId);
        if (endpoint == null) {
            throw new InvalidStateException(
                    "Failed to process custom socket connect message. Endpoint not found for client: " + clientId);
        }

        return processConnection(endpoint, socketId, socket);
    }

    /**
     * Processes a common message.
     * For clients that don't follow multi socket protocol endpoint is created on first message.
     *
     * @param message
     * @param socket
     */
    public void processCommonMessage(final String message, final Socket socket) throws InvalidStateException {
        SimpleServerEndpoint endpoint = endpointBySocket.get(socket);
        if (endpoint == null) {
            endpoint = createSingleSocketEndpoint(socket);
            endpointBySocket.put(socket, endpoint);
            endpoint.notifyConnectionEstablished();
            endpoint.connectionEstablished();
        }
        endpoint.notifyMessageReceived(message);
    }

    /**
     * Notifies server that connection was established on the endpoint of the given {@link Socket}
     *
     * @param socket the last connected socket
     */
    public void allConnectionsEstablished(final Socket socket) throws InvalidStateException {
        final SimpleServerEndpoint endpoint = endpointBySocket.get(socket);
        if (endpoint != null) {
            endpoint.notifyConnectionEstablished();
            if (!endpoint.isEstablished()) {
                endpoint.connectionEstablished();
            } else {
                endpoint.reconnectionSuccessful();
            }

        } else {
            throw new InvalidStateException(
                    "Failed to notify connection established. Session not found for socket: " + socket);
        }
    }

    /**
     * Called when a socket was closed.
     * Executes a task on a different thread, which clears old sockets and disconnects endpoint.
     */
    public void socketClosed(final Socket closedSocket) {
        processClose(closedSocket);
    }

    private void processClose(final Socket socket) {
        closeLock.lock();
        try {
            final SimpleServerEndpoint endpoint = endpointBySocket.get(socket);
            if (endpoint != null) {
                executeClose(endpoint, socket);
            }
        } finally {
            closeLock.unlock();
        }
    }

    private void processClose(final SimpleServerEndpoint endpoint) {
        closeLock.lock();
        try {
            Socket socket = null;
            for (Map.Entry<Socket, SimpleServerEndpoint> entry : endpointBySocket.entrySet()) {
                if (entry.getValue().equals(endpoint)) {
                    socket = entry.getKey();
                    break;
                }
            }
            if (socket != null) {
                executeClose(endpoint, socket);
            }
        } finally {
            closeLock.unlock();
        }
    }

    private void executeClose(SimpleServerEndpoint endpoint, Socket socket) {
        clearSockets(endpoint);
        if (endpoint.isConnected()) {
            endpoint.disconnect();
            LOGGER.info("Endpoint disconnected.");
        }
        endpoint.connectionClosed();
        final boolean multiSocket = endpointByClientId.containsValue(endpoint);
        if (multiSocket) {
            establishedConnectionsByEndpoint.get(endpoint).set(0);
            final long closeTime = System.currentTimeMillis();
            lastCloseTimeByEndpoint.put(endpoint, closeTime);
            scheduleCleanUpTask(socket, endpoint, closeTime);
        }
    }

    private void scheduleCleanUpTask(
            final Socket socket, final SimpleServerEndpoint endpoint, final long closeTime
    ) {
        socket.getEventLoop().schedule(
                cleanUpTask(endpoint, closeTime),
                Resources.SERVER_CLEANUP_TIMEOUT_MILLIS,
                TimeUnit.MILLISECONDS
        );
    }

    private Runnable cleanUpTask(final SimpleServerEndpoint endpoint, final long closeTime) {
        return new Runnable() {
            @Override
            public void run() {
                if (lastCloseTimeByEndpoint.get(endpoint).equals(closeTime)
                        && establishedConnectionsByEndpoint.get(endpoint).equals(0)) {
                    lastCloseTimeByEndpoint.remove(endpoint);
                    establishedConnectionsByEndpoint.remove(endpoint);
                    subjectsDispatcherByEndpoint.remove(endpoint);
                    Iterator<SimpleServerEndpoint> iterator = endpointByClientId.values().iterator();
                    while (iterator.hasNext()) {
                        final SimpleServerEndpoint serverEndpoint = iterator.next();
                        if (serverEndpoint.equals(endpoint)) {
                            iterator.remove();
                            break;
                        }
                    }
                }
            }
        };
    }

    /**
     * Creates a new multi-socket endpoint instance.
     *
     * @param clientId
     * @param masterSocket
     * @param timeout
     * @return
     */
    private SimpleServerEndpoint createMultiSocketEndpoint(
            final String clientId, final Socket masterSocket, final int timeout
    ) throws InvalidStateException {
        final SubjectsDispatcher<Socket> subjectsDispatcher = new SubjectsDispatcher<Socket>();
        subjectsDispatcher.init(
                context.getSubjectsRouting().getDefaultRoute(), context.getSubjectsRouting().getRoutingTable()
        );
        final MessageDispatcher messageDispatcher = new MessageDispatcher(subjectsDispatcher);
        final SimpleServerEndpoint serverEndpoint = createEndpoint(masterSocket, clientId, messageDispatcher);
        subjectsDispatcherByEndpoint.put(serverEndpoint, subjectsDispatcher);
        serverEndpoint.initIdleHandler(idleHandlerFactory, timeout, clientId);
        return serverEndpoint;
    }

    /**
     * Creates a new single-socket endpoint instance.
     *
     * @param masterSocket
     * @return
     */
    private SimpleServerEndpoint createSingleSocketEndpoint(final Socket masterSocket) {
        final MessageDispatcher messageDispatcher = new MessageDispatcher();
        return createEndpoint(masterSocket, Resources.OLD_SOCKET_CLIENT_ID, messageDispatcher);
    }

    /**
     * Creates a new endpoint instance.
     *
     * @param masterSocket
     * @return
     */
    private SimpleServerEndpoint createEndpoint(final Socket masterSocket, final String clientId, final MessageDispatcher messageDispatcher) {
        final SimpleServerEndpoint serverEndpoint = new SimpleServerEndpoint(clientId, messageDispatcher, endpointListener);
        serverEndpoint.setMasterSocket(masterSocket);
        return serverEndpoint;
    }

    private ConnectionsStatus processConnection(
            final SimpleServerEndpoint endpoint, final String socketId, final Socket socket
    ) {
        final int routingTableSize = context.getSubjectsRouting().collapseRoutingTableBySocket().size();
        socket.setSocketId(socketId);
        subjectsDispatcherByEndpoint.get(endpoint).linkSocketToProcessor(socketId, socket);
        endpoint.addSocket(socket);
        AtomicInteger counter = establishedConnectionsByEndpoint.get(endpoint);
        endpointBySocket.put(socket, endpoint);
        if (counter.incrementAndGet() == routingTableSize) {
            return ConnectionsStatus.ALL_CONNECTIONS_ESTABLISHED;
        }
        return ConnectionsStatus.CONNECTIONS_IN_PROGRESS;
    }

    private void clearSockets(SimpleServerEndpoint simpleServerEndpoint) {
        Iterator<SimpleServerEndpoint> iterator = endpointBySocket.values().iterator();
        while (iterator.hasNext()) {
            SimpleServerEndpoint endpoint = iterator.next();
            if (endpoint.equals(simpleServerEndpoint)) {
                iterator.remove();
            }
        }
    }

    protected boolean isEndpointConnected(final Socket socket) {
        SimpleServerEndpoint serverEndpoint = endpointBySocket.get(socket);
        if(serverEndpoint != null) {
            return serverEndpoint.isConnected();
        }
        return false;
    }

    /**
     * @return the defaultRoute
     */
    protected String getDefaultRoute() {
        return context.getSubjectsRouting().getDefaultRoute();
    }

    /**
     * @return the routing table
     */
    protected String getRoutingTable() {
        return context.getSubjectsRouting().packRoutingTable();
    }
}
