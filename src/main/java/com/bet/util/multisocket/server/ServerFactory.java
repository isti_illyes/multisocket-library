package com.bet.util.multisocket.server;

import com.bet.util.multisocket.common.endpoint.EndpointListener;
import com.bet.util.multisocket.common.handler.IdleHandlerFactory;

/**
 * User: istvan.illyes
 * Date: 2/5/14
 */
public final class ServerFactory extends AbstractServerFactory {

    public ServerFactory() {
        super(new IdleHandlerFactory());
    }

    @Override
    public Server create(ServerContext serverContext, EndpointListener<ServerEndpoint> endpointListener) {
        return new Server(serverContext, endpointListener, getIdleHandlerFactory());
    }
}
