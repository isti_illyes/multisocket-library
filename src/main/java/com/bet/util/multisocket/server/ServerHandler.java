package com.bet.util.multisocket.server;

import com.bet.util.multisocket.common.InvalidStateException;
import com.bet.util.multisocket.common.Socket;
import com.bet.util.multisocket.protocol.CustomConnectMessage;
import com.bet.util.multisocket.protocol.MasterConnectMessage;
import com.bet.util.multisocket.protocol.MessageFactory;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.apache.log4j.Logger;

/**
 * User: istvan.illyes
 * Date: 7/1/13
 * <p/>
 * Handler class responsible of handling different events that happen on a socket.
 * Events like message receiving and dispatching or notifications when a connection was closed.
 */
@ChannelHandler.Sharable
final class ServerHandler extends ChannelInboundHandlerAdapter {

    private static final Logger LOGGER = Logger.getLogger(ServerHandler.class);
    private final ServerEventsProcessor serverEventsProcessor;

    public ServerHandler(final ServerEventsProcessor serverEventsProcessor) {
        this.serverEventsProcessor = serverEventsProcessor;
    }

    /**
     * Called every time a new message is received on the channel.
     * Takes care of message processing and replying.
     *
     * @param ctx
     * @param msg
     * @throws Exception
     */
    @Override
    public void channelRead(final ChannelHandlerContext ctx, final Object msg) {
        final Socket socket = new Socket(ctx.channel());
        final String message = ((String) msg);

        if(serverEventsProcessor.isEndpointConnected(socket) && !MessageFactory.isPingMessage(message)) {
            try {
                serverEventsProcessor.processCommonMessage(message, socket);
            } catch (InvalidStateException ex) {
                forceClose(socket, ex);
            }
        } else {
            // received: master socket [client id, timeout]
            if (MessageFactory.MasterSocketConnect.isMasterSocketConnectMessage(message)) {
                final MasterConnectMessage connectMessage = MessageFactory.MasterSocketConnect.parseMessage(message);
                if (connectMessage == null) {
                    LOGGER.error("Invalid master socket connect message, closing socket...");
                    socket.close();
                    return;
                }

                try {
                    serverEventsProcessor.processMasterSocketConnectedMessage(socket, connectMessage.getClientId(), connectMessage.getTimeout());
                    // reply: [socket id + routing table]
                    sendReply(ctx, MessageFactory.SubjectsRouting.buildMessage(
                            serverEventsProcessor.getDefaultRoute(), serverEventsProcessor.getRoutingTable())
                    );
                } catch (InvalidStateException ex) {
                    forceClose(socket, ex);
                }

                // received: custom socket [socket id, client id]
            } else if (MessageFactory.CustomSocketConnect.isCustomSocketConnectMessage(message)) {
                final CustomConnectMessage connectMessage = MessageFactory.CustomSocketConnect.parseMessage(message);
                if (connectMessage == null) {
                    LOGGER.error("Invalid custom socket connect message, closing socket...");
                    socket.close();
                    return;
                }

                final ConnectionsStatus connectionsStatus;
                try {
                    connectionsStatus = serverEventsProcessor.processCustomSocketConnectedMessage(socket, connectMessage.getSocketId(), connectMessage.getClientId());
                    // reply: [all connections established]
                    if (connectionsStatus == ConnectionsStatus.ALL_CONNECTIONS_ESTABLISHED) {
                        sendReply(ctx, MessageFactory.AllConnectionsEstablished.buildMessage());
                        try {
                            serverEventsProcessor.allConnectionsEstablished(socket);
                        } catch (InvalidStateException ex) {
                            forceClose(socket, ex);
                        }
                    }
                } catch (InvalidStateException ex) {
                    forceClose(socket, ex);
                }

                // received: master socket [client id, timeout - reconnect]
            } else if (MessageFactory.MasterSocketReconnect.isMasterSocketReconnectedMessage(message)) {
                final MasterConnectMessage connectMessage = MessageFactory.MasterSocketReconnect.parseMessage(message);
                if (connectMessage == null) {
                    LOGGER.error("Invalid master socket reconnect message, closing socket...");
                    socket.close();
                    return;
                }

                try {
                    serverEventsProcessor.processMasterSocketReconnectedMessage(socket, connectMessage.getClientId(), connectMessage.getTimeout());
                    // reply: [ACK for reconnection]
                    sendReply(ctx, MessageFactory.MasterSocketReconnectAck.buildMessage());
                } catch (InvalidStateException ex) {
                    forceClose(socket, ex);
                }

                // common message
            } else if (!MessageFactory.isPingMessage(message)) {
                try {
                    serverEventsProcessor.processCommonMessage(message, socket);
                } catch (InvalidStateException ex) {
                    forceClose(socket, ex);
                }
            }
        }
    }

    /**
     * Called every time a channel is closed.
     * Notifies the processor that a channel was closed.
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        serverEventsProcessor.socketClosed(new Socket(ctx.channel()));
    }

    /**
     * Called every time an exception is raised in the handlers.
     *
     * @param ctx
     * @param cause
     * @throws java.lang.Exception
     */
    @Override
    public void exceptionCaught(final ChannelHandlerContext ctx, final Throwable cause) throws java.lang.Exception {
        LOGGER.error(String.format("Exception in %s", this.getClass().getName()), cause);
    }

    /**
     * Closes the socket and logs error message.
     *
     * @param socket
     * @param ex
     */
    private void forceClose(final Socket socket, final InvalidStateException ex) {
        LOGGER.error("Illegal server state, closing socket...", ex);
        socket.close();
    }

    /**
     * Sends a reply message.
     *
     * @param ctx
     * @param replyMessage
     */
    private void sendReply(final ChannelHandlerContext ctx, final String replyMessage) {
        ctx.writeAndFlush(replyMessage);
    }
}
