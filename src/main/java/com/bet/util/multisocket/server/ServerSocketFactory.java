package com.bet.util.multisocket.server;

import java.net.*;

import com.bet.util.multisocket.common.Resources;
import com.bet.util.multisocket.common.Socket;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import org.apache.log4j.Logger;

/**
 * User: istvan.illyes
 * Date: 1/20/14
 */
final class ServerSocketFactory {

    private static final Logger LOGGER = Logger.getLogger(ServerSocketFactory.class);

    /**
     * Creates an acceptor socket
     *
     * @param context
     * @param serverHandler
     * @return the {@link com.bet.util.multisocket.common.Socket}
     */
    protected static Socket createAcceptorSocket(final ServerContext context, final ServerHandler serverHandler) {
        final ServerBootstrap serverBootstrap = ServerBootstrapFactory.createServerBootstrap(context.getChannelType());

        serverBootstrap
                .childHandler(new ServerChannelInitializer(serverHandler))
                .option(ChannelOption.SO_BACKLOG, Resources.SO_BACKLOG);

        try {
            ChannelFuture channelFuture = serverBootstrap.bind(
                    new InetSocketAddress(context.getLocalAddress().getPort())).sync();
            channelFuture.awaitUninterruptibly();
            if (channelFuture.isSuccess()) {
                final Socket socket = new com.bet.util.multisocket.common.Socket(channelFuture.channel());
                return socket;

            } else {
                LOGGER.warn(String.format("Failed to open socket! Cannot bind to port: %d!",
                        context.getLocalAddress().getPort()));
            }
        } catch (InterruptedException e) {
            LOGGER.error("Failed to create acceptor socket.", e);
        }
        return null;
    }

    private static class ServerChannelInitializer extends ChannelInitializer<SocketChannel> {

        private ChannelHandler serverHandler;

        private ServerChannelInitializer(ChannelHandler serverHandler) {
            this.serverHandler = serverHandler;
        }

        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
            // Encoders
            ch.pipeline().addLast(Resources.STRING_ENCODER_NAME, Resources.STRING_ENCODER);

            // Decoders
            ch.pipeline().addLast(Resources.DELIMITER_DECODER_NAME,
                    new DelimiterBasedFrameDecoder(Integer.MAX_VALUE, Delimiters.lineDelimiter()));
            ch.pipeline().addLast(Resources.STRING_DECODER_NAME, Resources.STRING_DECODER);

            // Handlers
            ch.pipeline().addLast(Resources.LOGGING_HANDLER_NAME, Resources.LOGGING_HANDLER);
            ch.pipeline().addLast(Resources.SERVER_HANDLER_NAME, serverHandler);
        }
    }
}
