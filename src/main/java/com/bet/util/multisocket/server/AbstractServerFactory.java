package com.bet.util.multisocket.server;

import com.bet.util.multisocket.common.endpoint.EndpointListener;
import com.bet.util.multisocket.common.handler.AbstractIdleHandlerFactory;

/**
 * User: istvan.illyes
 * Date: 2/5/14
 */
public abstract class AbstractServerFactory {

    private AbstractIdleHandlerFactory idleHandlerFactory;

    public AbstractServerFactory(AbstractIdleHandlerFactory idleHandlerFactory) {
        this.idleHandlerFactory = idleHandlerFactory;
    }

    public abstract Server create(
            final ServerContext serverContext,
            final EndpointListener<ServerEndpoint> endpointListener
    );

    protected AbstractIdleHandlerFactory getIdleHandlerFactory() {
        return idleHandlerFactory;
    }
}
