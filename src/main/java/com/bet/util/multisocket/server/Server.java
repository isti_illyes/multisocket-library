package com.bet.util.multisocket.server;

import com.bet.util.multisocket.common.InvalidStateException;
import com.bet.util.multisocket.common.Socket;
import com.bet.util.multisocket.common.endpoint.EndpointListener;
import com.bet.util.multisocket.common.handler.AbstractIdleHandlerFactory;
import io.netty.channel.ChannelFuture;
import io.netty.util.internal.logging.InternalLoggerFactory;
import io.netty.util.internal.logging.Log4JLoggerFactory;
import org.apache.log4j.Logger;

/**
 * User: istvan.illyes
 * Date: 7/1/13
 */
public final class Server {

    private static final Logger LOGGER = Logger.getLogger(Server.class);

    private final ServerContext context;
    private EndpointListener<ServerEndpoint> endpointListener;
    private AbstractIdleHandlerFactory idleHandlerFactory;
    private Socket acceptorSocket;
    private volatile boolean started = false;

    protected Server(
            final ServerContext context,
            final EndpointListener<ServerEndpoint> endpointListener,
            final AbstractIdleHandlerFactory idleHandlerFactory
    ) {
        this.context = context;
        this.endpointListener = endpointListener;
        this.idleHandlerFactory = idleHandlerFactory;
    }

    /**
     * Creates an acceptor channel to listen on the given port.
     */
    public void start() throws InvalidStateException {
        if (started) {
            throw new InvalidStateException(String.format("Failed to start %s. Already started",
                    this.getClass().getName()));
        }
        started = true;
        InternalLoggerFactory.setDefaultFactory(new Log4JLoggerFactory());
        final ServerHandler serverHandler =
                new ServerHandler(
                        new ServerEventsProcessor(context, idleHandlerFactory, endpointListener)
                );
        acceptorSocket = ServerSocketFactory.createAcceptorSocket(context, serverHandler);
    }

    /**
     * Closes the acceptor socket of this server
     */
    public void stop() throws InterruptedException {
        started = false;
        if (acceptorSocket != null) {
            ChannelFuture channelFuture = acceptorSocket.close();
            channelFuture.await();
        }
        LOGGER.info("Server was stopped!");
    }
}
