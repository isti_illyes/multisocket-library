package com.bet.util.multisocket.server;

import java.net.*;

import com.bet.util.multisocket.common.ChannelType;
import com.bet.blues.proxy.socket.multi.SubjectsRouting;

/**
 * User: istvan.illyes
 * Date: 7/9/13
 */
public final class ServerContext {

    private final SubjectsRouting subjectsRouting;
    private final InetSocketAddress localAddress;
    private final ChannelType channelType;

    private ServerContext(final ServerContextBuilder builder) {
        this.subjectsRouting = builder.subjectsRouting;
        this.localAddress = builder.localAddress;
        this.channelType = builder.channelType;
    }

    public static ServerContextBuilder createBuilder() {
        return new ServerContextBuilder();
    }

    public SubjectsRouting getSubjectsRouting() {
        return subjectsRouting;
    }

    public InetSocketAddress getLocalAddress() {
        return localAddress;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public static final class ServerContextBuilder {

        private SubjectsRouting subjectsRouting;
        private InetSocketAddress localAddress;
        private ChannelType channelType;

        public ServerContextBuilder subjectsRouting(final SubjectsRouting subjectsRouting) {
            this.subjectsRouting = subjectsRouting;
            return this;
        }

        public ServerContextBuilder localAddress(final InetSocketAddress localAddress) {
            this.localAddress = localAddress;
            return this;
        }

        public ServerContextBuilder channelType(final ChannelType channelType) {
            this.channelType = channelType;
            return this;
        }

        public ServerContext build() {
            return new ServerContext(this);
        }
    }
}
