package com.bet.util.multisocket.server;

/**
 * User: mihai.vass
 * Date: 8/14/13
 */
public enum ConnectionsStatus {
    CONNECTIONS_IN_PROGRESS,
    ALL_CONNECTIONS_ESTABLISHED
}
