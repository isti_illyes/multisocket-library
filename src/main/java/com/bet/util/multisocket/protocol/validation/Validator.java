package com.bet.util.multisocket.protocol.validation;

import java.util.*;

/**
 * User: mihai.vass
 * Date: 8/13/13
 */
public class Validator {

    private List<ValidationRule> rules = new ArrayList<ValidationRule>();

    public void addRule(final ValidationRule rule) {
        rules.add(rule);
    }

    public boolean isValid() {
        for (ValidationRule rule : rules) {
            if (!rule.isValid()) {
                return false;
            }
        }

        return true;
    }
}
