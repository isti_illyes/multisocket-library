package com.bet.util.multisocket.protocol.validation;

/**
 * User: mihai.vass
 * Date: 8/13/13
 */
public class FieldFactory {

    public static Field create(final String name, final int index) {
        return new Field(name, index);
    }
}
