package com.bet.util.multisocket.protocol.validation;

/**
 * User: mihai.vass
 * Date: 8/13/13
 */
public abstract class ValidationRule {

    public boolean isValid() {
        final boolean validated = validate();
        if (!validated) {
            logError();
        }

        return validated;
    }

    protected abstract boolean validate();

    protected abstract void logError();
}
