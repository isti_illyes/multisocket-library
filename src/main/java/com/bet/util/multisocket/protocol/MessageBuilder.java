package com.bet.util.multisocket.protocol;

import java.util.*;

import com.bet.util.multisocket.common.Resources;
import org.apache.commons.lang.StringUtils;

/**
 * User: mihai.vass
 * Date: 8/12/13
 */
public class MessageBuilder {

    private String header;
    private List<Object> fields;
    private boolean appendMessageSeparator = true;

    public MessageBuilder header(final String header) {
        this.header = header;
        return this;
    }

    public MessageBuilder fields(final Object... fields) {
        this.fields = Arrays.asList(fields);
        return this;
    }

    public MessageBuilder messageSeparator(final boolean appendMessageSeparator) {
        this.appendMessageSeparator = appendMessageSeparator;
        return this;
    }

    public String build() {
        // header
        final StringBuilder builder = new StringBuilder(header);
        builder.append(Resources.MESSAGE_FIELD_SEPARATOR);

        // fields
        if (fields != null) {
            final Iterator<?> it = fields.iterator();
            if (it.hasNext()) {
                builder.append(it.next());
                while (it.hasNext()) {
                    builder.append(Resources.MESSAGE_FIELD_SEPARATOR).append(it.next());
                }
            }
        }

        // message separator
        if (appendMessageSeparator) {
            builder.append(Resources.MESSAGE_SEPARATOR);
        }

        return builder.toString();
    }

    public static String[] parseMessage(final String message) {
        if (StringUtils.isBlank(message)) {
            return new String[]{};
        }

        return message.split(Resources.MESSAGE_FIELD_SEPARATOR);
    }
}
