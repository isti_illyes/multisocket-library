package com.bet.util.multisocket.protocol;

/**
 * User: mihai.vass
 * Date: 8/12/13
 */
public class MasterConnectMessage {

    private final String clientId;
    private final int timeout;

    public MasterConnectMessage(final String clientId, final int timeout) {
        this.clientId = clientId;
        this.timeout = timeout;
    }

    public String getClientId() {
        return clientId;
    }

    public int getTimeout() {
        return timeout;
    }
}
