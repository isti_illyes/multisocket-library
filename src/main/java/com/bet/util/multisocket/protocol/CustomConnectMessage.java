package com.bet.util.multisocket.protocol;

/**
 * User: mihai.vass
 * Date: 8/12/13
 */
public class CustomConnectMessage {

    private final String clientId;
    private final String socketId;

    public CustomConnectMessage(final String clientId, final String socketId) {
        this.clientId = clientId;
        this.socketId = socketId;
    }

    public String getClientId() {
        return clientId;
    }

    public String getSocketId() {
        return socketId;
    }
}
