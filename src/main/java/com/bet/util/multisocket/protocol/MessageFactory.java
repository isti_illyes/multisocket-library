package com.bet.util.multisocket.protocol;

import com.bet.util.multisocket.common.Resources;
import com.bet.util.multisocket.protocol.validation.Field;
import com.bet.util.multisocket.protocol.validation.FieldFactory;
import org.apache.commons.lang.StringUtils;

/**
 * User: mihai.vass
 * Date: 8/12/13
 */
public class MessageFactory {

    public static class MasterSocketConnect {

        private static final String MESSAGE_DESCRIPTION = "master socket connect";
        private static final Field CLIENT_ID = FieldFactory.create("client id", 1);
        private static final Field TIMEOUT = FieldFactory.create("timeout", 2);
        private static final int NUMBER_OF_TOKENS = 3;

        public static String buildMessage(final String clientId, final int timeout) {
            return new MessageBuilder()
                    .header(Resources.MASTER_SOCKET_CONNECT_MESSAGE_HEADER)
                    .fields(clientId, timeout).build();
        }

        public static boolean isMasterSocketConnectMessage(final String message) {
            if (StringUtils.isEmpty(message)) {
                return false;
            }

            return message.startsWith(Resources.MASTER_SOCKET_CONNECT_MESSAGE_HEADER);
        }

        public static MasterConnectMessage parseMessage(final String message) {
            final MessageParser messageParser = new MessageParser()
                    .parse(MESSAGE_DESCRIPTION, message)
                    .wellFormed(NUMBER_OF_TOKENS)
                    .notBlank(CLIENT_ID)
                    .isInteger(TIMEOUT);

            if (!messageParser.isValid()) {
                return null;
            }

            return new MasterConnectMessage(messageParser.getField(CLIENT_ID.getIndex()),
                    Integer.parseInt(messageParser.getField(TIMEOUT.getIndex())));
        }
    }

    //TODO remove class
    public static class MasterSocketId {

        private static final String MESSAGE_DESCRIPTION = "master socket id";
        private static final Field SOCKET = FieldFactory.create("socket id", 1);
        private static final int NUMBER_OF_TOKENS = 2;

        public static String buildMessage(final String masterSocketId) {
            return new MessageBuilder()
                    .header(Resources.MASTER_SOCKET_ID_MESSAGE_HEADER)
                    .fields(masterSocketId).build();
        }

        public static boolean isMasterSocketIdMessage(final String message) {
            if (StringUtils.isEmpty(message)) {
                return false;
            }

            return message.startsWith(Resources.MASTER_SOCKET_ID_MESSAGE_HEADER);
        }

        public static String parseSocketIdFromMessage(final String message) {
            final MessageParser messageParser = new MessageParser()
                    .parse(MESSAGE_DESCRIPTION, message)
                    .wellFormed(NUMBER_OF_TOKENS)
                    .notBlank(SOCKET);

            if (!messageParser.isValid()) {
                return null;
            }

            return messageParser.getField(SOCKET.getIndex());
        }
    }

    //TODO remove class
    public static class MasterSocketIdAck {

        public static String buildMessage() {
            return new MessageBuilder()
                    .header(Resources.MASTER_SOCKET_ID_ACK_MESSAGE_HEADER).build();
        }

        public static boolean isMasterSocketIdAckMessage(final String message) {
            if (StringUtils.isEmpty(message)) {
                return false;
            }

            return message.startsWith(Resources.MASTER_SOCKET_ID_ACK_MESSAGE_HEADER);
        }
    }

    public static class SubjectsRouting {

        private static final String MESSAGE_DESCRIPTION = "routing table";
        private static final Field DEFAULT_ROUTE = FieldFactory.create("default route", 1);
        private static Field ROUTING_TABLE = FieldFactory.create("routing table", 2);
        private static final int NUMBER_OF_TOKENS = 3;

        public static String buildMessage(final String defaultRoute, final String routingTable) {
            return new MessageBuilder()
                    .header(Resources.SUBJECTS_ROUTING_MESSAGE_HEADER)
                    .fields(defaultRoute, routingTable)
                    .build();
        }

        public static boolean isRoutingTableMessage(final String message) {
            if (StringUtils.isEmpty(message)) {
                return false;
            }

            return message.startsWith(Resources.SUBJECTS_ROUTING_MESSAGE_HEADER);
        }

        public static com.bet.blues.proxy.socket.multi.SubjectsRouting parseMessage(final String message) {
            final MessageParser messageParser = new MessageParser()
                    .parse(MESSAGE_DESCRIPTION, message)
                    .wellFormed(NUMBER_OF_TOKENS)
                    .notBlank(DEFAULT_ROUTE)
                    .notBlank(ROUTING_TABLE);

            if (!messageParser.isValid()) {
                return null;
            }

            final com.bet.blues.proxy.socket.multi.SubjectsRouting subjectsRouting =
                    new com.bet.blues.proxy.socket.multi.SubjectsRouting();
            subjectsRouting.setDefaultRoute(messageParser.getField(DEFAULT_ROUTE.getIndex()));
            subjectsRouting.addRoutingInfo(
                    com.bet.blues.proxy.socket.multi.SubjectsRouting.unpackRoutingTable(
                            messageParser.getField(ROUTING_TABLE.getIndex()))
            );

            return subjectsRouting;
        }
    }

    public static class CustomSocketConnect {

        private static final String MESSAGE_DESCRIPTION = "custom socket connect";
        private static final Field CLIENT_ID = FieldFactory.create("client id", 1);
        private static final Field SOCKET_ID = FieldFactory.create("socket id", 2);
        private static final int NUMBER_OF_TOKENS = 3;

        public static String buildMessage(final String socketId, final String clientId) {
            return new MessageBuilder()
                    .header(Resources.CUSTOM_SOCKET_CONNECT_MESSAGE_HEADER)
                    .fields(clientId, socketId)
                    .build();
        }

        public static boolean isCustomSocketConnectMessage(final String message) {
            if (StringUtils.isEmpty(message)) {
                return false;
            }

            return message.startsWith(Resources.CUSTOM_SOCKET_CONNECT_MESSAGE_HEADER);
        }

        public static CustomConnectMessage parseMessage(final String message) {
            final MessageParser messageParser = new MessageParser()
                    .parse(MESSAGE_DESCRIPTION, message)
                    .wellFormed(NUMBER_OF_TOKENS)
                    .notBlank(CLIENT_ID)
                    .notBlank(SOCKET_ID);

            if (!messageParser.isValid()) {
                return null;
            }

            return new CustomConnectMessage(
                    messageParser.getField(CLIENT_ID.getIndex()), messageParser.getField(SOCKET_ID.getIndex()));
        }
    }

    public static class AllConnectionsEstablished {

        public static String buildMessage() {
            return new MessageBuilder()
                    .header(Resources.ALL_CONNECTIONS_ESTABLISHED_MESSAGE_HEADER).build();
        }

        public static boolean isAllConnectionsEstablishedMessage(final String message) {
            if (StringUtils.isEmpty(message)) {
                return false;
            }

            return message.startsWith(Resources.ALL_CONNECTIONS_ESTABLISHED_MESSAGE_HEADER);
        }
    }

    public static class MasterSocketReconnect {

        private static final String MESSAGE_DESCRIPTION = "master socket reconnect";
        private static final Field CLIENT_ID = FieldFactory.create("client id", 1);
        private static final Field TIMEOUT = FieldFactory.create("timeout", 2);
        private static final int NUMBER_OF_TOKENS = 3;

        public static String buildMessage(final String clientId, final int timeout) {
            return new MessageBuilder()
                    .header(Resources.MASTER_SOCKET_RECONNECT_MESSAGE_HEADER)
                    .fields(clientId, timeout).build();
        }

        public static boolean isMasterSocketReconnectedMessage(final String message) {
            if (StringUtils.isEmpty(message)) {
                return false;
            }

            return message.startsWith(Resources.MASTER_SOCKET_RECONNECT_MESSAGE_HEADER);
        }

        public static MasterConnectMessage parseMessage(final String message) {
            final MessageParser messageParser = new MessageParser()
                    .parse(MESSAGE_DESCRIPTION, message)
                    .wellFormed(NUMBER_OF_TOKENS)
                    .notBlank(CLIENT_ID)
                    .notBlank(TIMEOUT);

            if (!messageParser.isValid()) {
                return null;
            }

            return new MasterConnectMessage(
                    messageParser.getField(CLIENT_ID.getIndex()),
                    Integer.valueOf(messageParser.getField(TIMEOUT.getIndex()))
            );
        }
    }

    public static class MasterSocketReconnectAck {

        public static String buildMessage() {
            return new MessageBuilder()
                    .header(Resources.MASTER_SOCKET_RECONNECT_ACK_MESSAGE_HEADER).build();
        }

        public static boolean isReconnectionAckMessage(final String message) {
            if (StringUtils.isEmpty(message)) {
                return false;
            }

            return message.startsWith(Resources.MASTER_SOCKET_RECONNECT_ACK_MESSAGE_HEADER);
        }
    }

    public static boolean isPingMessage(final String message) {
        return message.startsWith(Resources.PING_MESSAGE_HEADER);
    }
}
