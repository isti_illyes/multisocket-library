package com.bet.util.multisocket.protocol;

import com.bet.util.multisocket.common.Resources;
import com.bet.util.multisocket.protocol.validation.Field;
import com.bet.util.multisocket.protocol.validation.ValidationRule;
import com.bet.util.multisocket.protocol.validation.Validator;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * User: mihai.vass
 * Date: 8/13/13
 */
public class MessageParser {

    private static final Logger LOGGER = Logger.getLogger(MessageParser.class);

    private String messageDescription;
    private String message;
    private String[] tokens;
    public Validator validator = new Validator();

    public MessageParser parse(final String messageDescription, final String message) {
        this.messageDescription = messageDescription;
        this.message = message;
        this.tokens = parseMessage(message);
        return this;
    }

    private static String[] parseMessage(final String message) {
        if (StringUtils.isBlank(message)) {
            return new String[]{};
        }

        return message.split(Resources.MESSAGE_FIELD_SEPARATOR);
    }

    public MessageParser wellFormed(final int numberOfTokens) {
        validator.addRule(new WellFormedValidationRule(numberOfTokens));
        return this;
    }

    public MessageParser notBlank(final Field field) {
        validator.addRule(new NotBlankValidationRule(field.getName(), field.getIndex()));
        return this;
    }

    public MessageParser isInteger(final Field field) {
        validator.addRule(new IsIntegerValidationRule(field.getName(), field.getIndex()));
        return this;
    }

    public boolean isValid() {
        return validator.isValid();
    }

    public String getField(final int fieldIndex) {
        return tokens[fieldIndex];
    }

    private class WellFormedValidationRule extends ValidationRule {

        private final int numberOfTokens;

        public WellFormedValidationRule(final int numberOfTokens) {
            this.numberOfTokens = numberOfTokens;
        }

        @Override
        protected boolean validate() {
            return tokens.length == numberOfTokens;
        }

        @Override
        protected void logError() {
            LOGGER.error("Invalid " + messageDescription + " message!");
        }
    }

    private class NotBlankValidationRule extends ValidationRule {

        private final String fieldName;
        private final int fieldIndex;

        public NotBlankValidationRule(final String fieldName, final int fieldIndex) {
            this.fieldIndex = fieldIndex;
            this.fieldName = fieldName;
        }

        @Override
        protected boolean validate() {
            return StringUtils.isNotBlank(tokens[fieldIndex]);
        }

        @Override
        protected void logError() {
            LOGGER.error(String.format("Invalid %s message: '%s', %s is blank.", messageDescription, message.trim(), fieldName));
        }
    }

    private class IsIntegerValidationRule extends ValidationRule {

        private final String fieldName;
        private final int fieldIndex;

        public IsIntegerValidationRule(final String fieldName, final int fieldIndex) {
            this.fieldName = fieldName;
            this.fieldIndex = fieldIndex;
        }

        @Override
        protected boolean validate() {
            final String fieldValue = tokens[fieldIndex];
            return StringUtils.isNotBlank(fieldValue) && isInteger(fieldValue);
        }

        @Override
        protected void logError() {
            LOGGER.error(String.format("Invalid %s message: '%s', %s has invalid value.", messageDescription, message.trim(), fieldName));
        }

        private boolean isInteger(final String timeout) {
            try {
                Integer.parseInt(timeout);
                return true;

            } catch (NumberFormatException e) {
                return false;
            }
        }
    }
}
