package com.bet.util.multisocket.protocol.validation;

/**
 * User: mihai.vass
 * Date: 8/13/13
 */
public class Field {

    private String name;
    private int index;

    public Field(final String name, final int index) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public int getIndex() {
        return index;
    }
}
