package com.bet.util.multisocket.common.handler;

import com.bet.util.multisocket.common.Resources;
import com.bet.util.multisocket.common.heartbeat.HeartbeatEventListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;

/**
 * User: istvan.illyes
 * Date: 7/1/13
 */
public final class IdleChannelHandler extends AbstractIdleChannelHandler {

    IdleChannelHandler(final HeartbeatEventListener stateEvent, final String socketId,
                       final String clientId, final IdleHandlerContext context) {
        super(stateEvent, socketId, clientId, context);
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent e = (IdleStateEvent) evt;
            if (e.state().equals(IdleState.WRITER_IDLE)) {
                ctx.writeAndFlush(Resources.PING_MESSAGE_HEADER);

            } else {
                super.userEventTriggered(ctx, evt);
            }
        }
    }
}
