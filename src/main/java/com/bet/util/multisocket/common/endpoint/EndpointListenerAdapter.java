package com.bet.util.multisocket.common.endpoint;

import com.bet.util.multisocket.common.ConnectionStatusListener;
import com.bet.util.multisocket.common.heartbeat.HeartbeatEventListener;

/**
 * User: istvan.illyes
 * Date: 1/21/14
 */
public final class EndpointListenerAdapter<T> implements HeartbeatEventListener, ConnectionStatusListener {

    private EndpointListener<T> endpointListener;
    private T endpoint;

    public EndpointListenerAdapter(EndpointListener<T> endpointListener, T endpoint) {
        this.endpointListener = endpointListener;
        this.endpoint = endpoint;
    }

    public void connectionEstablished() {
        endpointListener.connectionEstablished(endpoint);
    }

    @Override
    public void connectionFailed() {
        endpointListener.connectionFailed();
    }

    @Override
    public void connectionClosed() {
        endpointListener.connectionClosed();
    }

    @Override
    public void reconnectionSuccessful() {
        endpointListener.reconnectionSuccessful();
    }

    @Override
    public void reconnectionFailed() {
        endpointListener.reconnectionFailed();
    }

    @Override
    public void notifyHeartbeatRecovered(String socketId, String clientId) {
        endpointListener.notifyHeartbeatRecovered(socketId, clientId);
    }

    @Override
    public void notifyHeartbeatLate(String socketId, String clientId) {
        endpointListener.notifyHeartbeatLate(socketId, clientId);
    }

    @Override
    public void notifyHeartbeatLost(String socketId, String clientId) {
        endpointListener.notifyHeartbeatLost(socketId, clientId);
    }

    @Override
    public void notifyHeartbeatDead(String socketId, String clientId) {
        endpointListener.notifyHeartbeatDead(socketId, clientId);
    }
}
