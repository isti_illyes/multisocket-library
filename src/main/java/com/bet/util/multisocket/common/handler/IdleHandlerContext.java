package com.bet.util.multisocket.common.handler;

/**
 * User: istvan.illyes
 * Date: 7/9/13
 */
public final class IdleHandlerContext {

    private final int readerIdleLimit;
    private final int writerIdleLimit;
    private final int allIdleLimit;
    private final int lateTimeLimit;
    private final int lostTimeLimit;
    private final int deadTimeLimit;

    private IdleHandlerContext(IdleHandlerTimeLimitContextBuilder builder) {
        this.readerIdleLimit = builder.readerIdleLimit;
        this.writerIdleLimit = builder.writerIdleLimit;
        this.allIdleLimit = builder.allIdleLimit;
        this.lateTimeLimit = builder.lateTimeLimit;
        this.lostTimeLimit = builder.lostTimeLimit;
        this.deadTimeLimit = builder.deadTimeLimit;
    }

    public static IdleHandlerTimeLimitContextBuilder createBuilder() {
        return new IdleHandlerTimeLimitContextBuilder();
    }

    public int getReaderIdleLimit() {
        return readerIdleLimit;
    }

    public int getWriterIdleLimit() {
        return writerIdleLimit;
    }

    public int getAllIdleLimit() {
        return allIdleLimit;
    }

    public int getLateTimeLimit() {
        return lateTimeLimit;
    }

    public int getLostTimeLimit() {
        return lostTimeLimit;
    }

    public int getDeadTimeLimit() {
        return deadTimeLimit;
    }

    public static class IdleHandlerTimeLimitContextBuilder {
        private int readerIdleLimit;
        private int writerIdleLimit;
        private int allIdleLimit;
        private int lateTimeLimit;
        private int lostTimeLimit;
        private int deadTimeLimit;

        public IdleHandlerTimeLimitContextBuilder readerIdleLimit(int readerIdleLimit) {
            this.readerIdleLimit = readerIdleLimit;
            return this;
        }

        public IdleHandlerTimeLimitContextBuilder writerIdleLimit(int writerIdleLimit) {
            this.writerIdleLimit = writerIdleLimit;
            return this;
        }

        public IdleHandlerTimeLimitContextBuilder allIdleLimit(int allIdleLimit) {
            this.allIdleLimit = allIdleLimit;
            return this;
        }

        public IdleHandlerTimeLimitContextBuilder lateTimeLimit(int lateTimeLimit) {
            this.lateTimeLimit = lateTimeLimit;
            return this;
        }

        public IdleHandlerTimeLimitContextBuilder lostTimeLimit(int lostTimeLimit) {
            this.lostTimeLimit = lostTimeLimit;
            return this;
        }

        public IdleHandlerTimeLimitContextBuilder deadTimeLimit(int deadTimeLimit) {
            this.deadTimeLimit = deadTimeLimit;
            return this;
        }

        public IdleHandlerContext build() {
            return new IdleHandlerContext(this);
        }
    }
}
