package com.bet.util.multisocket.common;

/**
 * User: istvan.illyes
 * Date: 1/29/14
 */
public interface Reconnectable {

    public void reconnect() throws InvalidStateException;
}
