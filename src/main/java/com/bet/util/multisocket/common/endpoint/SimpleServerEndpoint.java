package com.bet.util.multisocket.common.endpoint;

import java.util.concurrent.*;
import java.util.concurrent.locks.*;

import com.bet.util.multisocket.common.InvalidStateException;
import com.bet.util.multisocket.common.MessageDispatcher;
import com.bet.util.multisocket.common.Resources;
import com.bet.util.multisocket.common.heartbeat.HeartbeatEventListener;
import com.bet.util.multisocket.server.ServerEndpoint;
import org.apache.log4j.Logger;

/**
 * User: istvan.illyes
 * Date: 1/29/14
 */
public final class SimpleServerEndpoint
        extends Endpoint<ServerEndpoint> implements HeartbeatEventListener {

    private static final Logger LOGGER = Logger.getLogger(SimpleServerEndpoint.class);
    private static final ScheduledExecutorService SCHEDULED_EXECUTOR = Executors.newSingleThreadScheduledExecutor();
    private final Lock lock = new ReentrantLock();
    private volatile boolean heartbeatDead;
    private long lastDisconnect;

    public SimpleServerEndpoint(
            final String clientId,
            final MessageDispatcher messageDispatcher,
            final EndpointListener<ServerEndpoint> endpointListener
    ) {
        super(clientId, messageDispatcher, endpointListener);
    }

    @Override
    public void notifyConnectionEstablished() throws InvalidStateException {
        super.notifyConnectionEstablished();
        heartbeatDead = false;
    }

    @Override
    protected ServerEndpoint createEndpoint() {
        return new ServerEndpoint(this);
    }

    @Override
    public void disconnect() {
        lock.lock();
        try {
            super.disconnect();
            if(!isClosed()) {
                lastDisconnect = System.currentTimeMillis();
                SCHEDULED_EXECUTOR.schedule(
                        closeTask(lastDisconnect), Resources.SERVER_ENDPOINT_CLOSE_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS
                );
            }
        } finally {
            lock.unlock();
        }
    }

    private Runnable closeTask(final long lastDisconnect) {
        return new Runnable() {
            @Override
            public void run() {
                if(!isConnected() && SimpleServerEndpoint.this.lastDisconnect == lastDisconnect) {
                    fireClose();
                }
            }
        };
    }

    @Override
    public void notifyHeartbeatDead(String socketId, String clientId) {
        super.notifyHeartbeatDead(socketId, clientId);
        processHeartbeatDead();
        LOGGER.info("Heartbeat dead.");
    }

    private void processHeartbeatDead() {
        lock.lock();
        try {
            if (!heartbeatDead) {
                heartbeatDead = true;
                disconnect();
            }
        } finally {
            lock.unlock();
        }
    }
}
