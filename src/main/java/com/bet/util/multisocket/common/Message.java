package com.bet.util.multisocket.common;

/**
 * User: istvan.illyes
 * Date: 2/12/14
 */
public final class Message {

    private final String message;
    private final String subject;

    public Message(String message, String subject) {
        this.message = message;
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public String getSubject() {
        return subject;
    }
}
