package com.bet.util.multisocket.common.handler;

import java.util.*;

import com.bet.util.multisocket.common.Resources;
import com.bet.util.multisocket.common.Socket;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.compression.JdkZlibDecoder;
import io.netty.handler.codec.compression.JdkZlibEncoder;
import org.apache.log4j.Logger;

/**
 * User: istvan.illyes
 * Date: 8/13/13
 */
public class CompressionHandler {

    private static final Logger LOGGER = Logger.getLogger(CompressionHandler.class);

    /**
     * Adds compression handlers to the handler pipelines of the given sockets
     *
     * @param sockets
     */
    public static void initHandler(final Collection<Socket> sockets) {
        for (Socket socket : sockets) {
            ChannelPipeline p = socket.getPipeline();
            if (p.get(Resources.COMPRESSION_ENCODER_NAME) == null && p.get(Resources.COMPRESSION_DECODER_NAME) == null
                    && p.get(Resources.FRAME_ENCODER_NAME) == null && p.get(Resources.FRAME_DECODER_NAME) == null
                    ) {
                // Encoders
                p.addBefore(Resources.STRING_ENCODER_NAME, Resources.COMPRESSION_ENCODER_NAME, new JdkZlibEncoder());

                // Decoders
                p.addBefore(Resources.STRING_DECODER_NAME, Resources.COMPRESSION_DECODER_NAME, new JdkZlibDecoder());

            } else {
                LOGGER.warn(
                        String.format("Failed to initialize %s and %s. Already initialized.",
                                Resources.COMPRESSION_ENCODER_NAME, Resources.COMPRESSION_DECODER_NAME)
                );
            }
        }
    }
}
