package com.bet.util.multisocket.common.handler;

import java.util.*;

import com.bet.util.multisocket.common.Resources;
import com.bet.util.multisocket.common.Socket;
import com.bet.util.multisocket.common.heartbeat.HeartbeatEventListener;

/**
 * This handler will act as HeartbeatManager.
 * User: istvan.illyes
 * Date: 7/30/13
 */
public abstract class AbstractIdleHandler {

    private final IdleHandlerContext idleHandlerContext;
    private final HeartbeatEventListener heartbeatEventListener;
    private final String clientId;

    /**
     * @param timeoutMillis          An integer value used to determine the limits for heartbeat events (late, lost, dead).
     * @param heartbeatEventListener The heartbeat event listener used by the idle handler
     * @param clientId
     * @return
     */
    protected AbstractIdleHandler(
            final int timeoutMillis,
            final HeartbeatEventListener heartbeatEventListener,
            final String clientId
    ) {
        this.idleHandlerContext = createIdleHandlerContext(timeoutMillis);
        this.heartbeatEventListener = heartbeatEventListener;
        this.clientId = clientId;
    }

    private IdleHandlerContext createIdleHandlerContext(int timeoutMillis) {
        return IdleHandlerContext
                .createBuilder()
                .readerIdleLimit((int) (timeoutMillis * Resources.READER_IDLE_MULTIPLIER))
                .writerIdleLimit((int) (timeoutMillis * Resources.WRITER_IDLE_MULTIPLIER))
                .allIdleLimit(Resources.ALL_IDLE_TIME_LIMIT)
                .lateTimeLimit((int) (timeoutMillis * Resources.HEARTBEAT_LATE_MULTIPLIER))
                .lostTimeLimit((int) (timeoutMillis * Resources.HEARTBEAT_LOST_MULTIPLIER))
                .deadTimeLimit((int) (timeoutMillis * Resources.HEARTBEAT_DEAD_MULTIPLIER))
                .build();
    }

    /**
     * Adds {@code IdleStateHandler} and {@code IdleChannelHandler} to the handler pipelines of the given sockets
     *
     * @param sockets - collection containing the {@code Socket}s
     */
    public abstract void initHandler(Collection<Socket> sockets);

    protected IdleHandlerContext getIdleHandlerContext() {
        return idleHandlerContext;
    }

    protected HeartbeatEventListener getHeartbeatEventListener() {
        return heartbeatEventListener;
    }

    protected String getClientId() {
        return clientId;
    }
}
