package com.bet.util.multisocket.common.endpoint;

import java.util.concurrent.locks.*;

import com.bet.util.multisocket.client.AutoReconnectingClientEndpoint;
import com.bet.util.multisocket.client.ReconnectionManager;
import com.bet.util.multisocket.common.InvalidStateException;
import com.bet.util.multisocket.common.MessageDispatcher;
import com.bet.util.multisocket.common.Resources;
import org.apache.log4j.Logger;

/**
 * Created by isti on 1/12/14.
 */
public class AutoReconnectingEndpoint
        extends Endpoint<AutoReconnectingClientEndpoint> {

    private static final Logger LOGGER = Logger.getLogger(AutoReconnectingEndpoint.class);
    private final ReconnectionManager reconnectionManager;
    private final Lock lock = new ReentrantLock();
    private volatile boolean heartbeatLost;

    public AutoReconnectingEndpoint(
            final String clientId,
            final MessageDispatcher messageDispatcher,
            final EndpointListener<AutoReconnectingClientEndpoint> endpointListener,
            final ReconnectionManager reconnectionManager
    ) {
        super(clientId, messageDispatcher, endpointListener);
        this.reconnectionManager = reconnectionManager;
    }

    @Override
    public void notifyConnectionEstablished() throws InvalidStateException {
        super.notifyConnectionEstablished();
        reconnectionManager.notifyConnectionEstablished();
        LOGGER.info(String.format("Connection established. Endpoint: %s", this));
        heartbeatLost = false;
    }

    @Override
    public void disconnect() {
        lock.lock();
        try {
            super.disconnect();
            if (!isClosed()) {
                boolean reconnected = false;
                try {
                    reconnected = reconnectionManager.tryReconnect(Resources.NO_RECONNECT_RETRIES);
                } catch (InvalidStateException e) {
                    LOGGER.warn("Failed to reconnect", e);
                }
                if (!reconnected) {
                    fireClose();
                }
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    protected AutoReconnectingClientEndpoint createEndpoint() {
        return new AutoReconnectingClientEndpoint(this);
    }

    @Override
    public void notifyHeartbeatLost(String socketId, String clientId) {
        super.notifyHeartbeatLost(socketId, clientId);
        processHeartbeatLost();
        LOGGER.info("Heartbeat lost.");
    }

    private void processHeartbeatLost() {
        lock.lock();
        try {
            if (!heartbeatLost) {
                heartbeatLost = true;
                disconnect(); // close sockets and try to reconnect
            }
        } finally {
            lock.unlock();
        }
    }
}