package com.bet.util.multisocket.common;

/**
 * User: istvan.illyes
 * Date: 2/11/14
 */
public interface ReconnectObserver {

    public void notifyReconnect();
}
