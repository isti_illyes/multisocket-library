package com.bet.util.multisocket.common.endpoint;

import java.util.concurrent.*;
import java.util.concurrent.locks.*;

import com.bet.util.multisocket.client.ManualReconnectingClientEndpoint;
import com.bet.util.multisocket.client.ReconnectionManager;
import com.bet.util.multisocket.common.InvalidStateException;
import com.bet.util.multisocket.common.MessageDispatcher;
import com.bet.util.multisocket.common.Reconnectable;
import org.apache.log4j.Logger;

/**
 * Created by isti on 1/12/14.
 */
public class ManualReconnectingEndpoint
        extends Endpoint<ManualReconnectingClientEndpoint> implements Reconnectable {

    private static final Logger LOGGER = Logger.getLogger(ManualReconnectingEndpoint.class);
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();
    private final ReconnectionManager reconnectionManager;
    private final long reconnectionTimeout;
    private final Object reconnectionLock = new Object();
    private final Lock lock = new ReentrantLock();
    private volatile boolean heartbeatLost;
    private volatile boolean reconnected = false;

    public ManualReconnectingEndpoint(
            final String clientId,
            final MessageDispatcher messageDispatcher,
            final EndpointListener<ManualReconnectingClientEndpoint> endpointListener,
            final ReconnectionManager reconnectionManager,
            final long reconnectionTimeout
    ) {
        super(clientId, messageDispatcher, endpointListener);
        this.reconnectionManager = reconnectionManager;
        this.reconnectionTimeout = reconnectionTimeout;
    }

    @Override
    public void notifyConnectionEstablished() throws InvalidStateException {
        super.notifyConnectionEstablished();
        reconnectionManager.notifyConnectionEstablished();
        heartbeatLost = false;
    }

    @Override
    public void disconnect() {
        lock.lock();
        try {
            super.disconnect();
            if (!isClosed()) {
                fireClose();
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    protected ManualReconnectingClientEndpoint createEndpoint() {
        return new ManualReconnectingClientEndpoint(this);
    }

    @Override
    public void close() {
        super.disconnect();
        EXECUTOR_SERVICE.execute(waitForReconnect());
    }

    private Runnable waitForReconnect() {
        return new Runnable() {
            @Override
            public void run() {
                long timeToWait = reconnectionTimeout;
                long start, end, slept;
                synchronized (reconnectionLock) {
                    while (timeToWait > 0) {
                        start = System.currentTimeMillis();
                        try {
                            reconnectionLock.wait(timeToWait);
                        } catch (InterruptedException e) {
                            LOGGER.warn("Exception caught while closing" + e);
                        }
                        if (reconnected) {
                            // client reconnected -> no need to wait anymore
                            break;
                        }
                        end = System.currentTimeMillis();
                        slept = end - start;
                        timeToWait -= slept;
                    }
                    // client didn't reconnect -> will close endpoint
                    if (!reconnected) {
                        ManualReconnectingEndpoint.super.close();
                        LOGGER.info("Closed endpoint.");
                    }
                    reconnected = false;
                }
            }
        };
    }

    @Override
    public void notifyHeartbeatLost(String socketId, String clientId) {
        super.notifyHeartbeatLost(socketId, clientId);
        processHeartbeatLost();
        LOGGER.info("Heartbeat lost.");
    }

    private void processHeartbeatLost() {
        lock.lock();
        try {
            if (!heartbeatLost) {
                heartbeatLost = true;
                disconnect();
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void reconnect() throws InvalidStateException {
        synchronized (reconnectionLock) {
            super.disconnect();
            reconnected = reconnectionManager.tryReconnect(1);
            reconnectionLock.notifyAll();
        }
    }
}
