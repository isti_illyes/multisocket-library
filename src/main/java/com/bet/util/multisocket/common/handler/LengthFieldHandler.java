package com.bet.util.multisocket.common.handler;

import com.bet.util.multisocket.common.Resources;
import com.bet.util.multisocket.common.Socket;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.compression.JdkZlibDecoder;
import io.netty.handler.codec.compression.JdkZlibEncoder;
import org.apache.log4j.Logger;

import java.util.Collection;

/**
 * Created by isti on 2/23/14.
 */
public class LengthFieldHandler {

    private static final Logger LOGGER = Logger.getLogger(LengthFieldHandler.class);


    private LengthFieldHandler() {
    }

    /**
     * Adds length field handlers to the handler pipelines of the given sockets
     *
     * @param sockets
     */
    public static void initHandler(final Collection<Socket> sockets) {
        for (Socket socket : sockets) {
            ChannelPipeline p = socket.getPipeline();
            if (p.get(Resources.COMPRESSION_ENCODER_NAME) == null && p.get(Resources.COMPRESSION_DECODER_NAME) == null
                    && p.get(Resources.FRAME_ENCODER_NAME) == null && p.get(Resources.FRAME_DECODER_NAME) == null
                    ) {
                // Encoders
                p.addBefore(Resources.STRING_ENCODER_NAME, Resources.FRAME_ENCODER_NAME,
                        new LengthFieldPrepender(Resources.FRAME_LENGTH_FIELD_SIZE));

                // Decoders
                p.addBefore(Resources.STRING_DECODER_NAME, Resources.FRAME_DECODER_NAME,
                        new LengthFieldBasedFrameDecoder(Resources.MAX_FRAME_LENGTH,
                                Resources.FRAME_LENGTH_FIELD_OFFSET, Resources.FRAME_LENGTH_FIELD_SIZE,
                                Resources.FRAME_LENGTH_ADJUSTMENT, Resources.FRAME_LENGTH_FIELD_SIZE));

            } else {
                LOGGER.warn(
                        String.format("Failed to initialize %s and %s. Already initialized.",
                                Resources.FRAME_ENCODER_NAME, Resources.FRAME_DECODER_NAME)
                );
            }
        }
    }
}
