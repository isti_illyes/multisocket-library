package com.bet.util.multisocket.common.endpoint;

import java.net.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.locks.*;

import com.bet.util.multisocket.common.InvalidStateException;
import com.bet.util.multisocket.common.Message;
import com.bet.util.multisocket.common.MessageDispatcher;
import com.bet.util.multisocket.common.Resources;
import com.bet.util.multisocket.common.Socket;
import com.bet.util.multisocket.common.handler.*;
import com.bet.util.multisocket.common.heartbeat.HeartbeatEventListener;
import com.bet.util.multisocket.common.heartbeat.HeartbeatEventListenerImpl;
import org.apache.log4j.Logger;

/**
 * User: istvan.illyes
 * Date: 1/12/14
 */
public abstract class Endpoint<T> implements EndpointListener<T> {

    private static final Logger LOGGER = Logger.getLogger(Endpoint.class);
    private final Lock lock = new ReentrantLock();
    private final BlockingQueue<String> readMessages = new LinkedBlockingQueue<String>();
    private final Queue<Message> messagesToSend = new LinkedBlockingQueue<Message>();
    private final List<Socket> sockets = new ArrayList<Socket>();
    private final String clientId;
    private final MessageDispatcher messageDispatcher;
    private EndpointListener<T> endpointListener;
    private AbstractIdleHandler idleHandler;
    private volatile boolean closed = false;
    private volatile boolean connected = false;
    private volatile boolean established = false;
    private Socket masterSocket;

    public Endpoint(
            final String clientId,
            final MessageDispatcher messageDispatcher,
            final EndpointListener<T> endpointListener
    ) {
        this.clientId = clientId;
        this.messageDispatcher = messageDispatcher;
        this.endpointListener = endpointListener;
    }

    public void sendMessage(final String message, final String subject) throws InvalidStateException {
        lock.lock();
        try {
            if (connected) {
                messageDispatcher.sendMessage(message, subject);

            } else {
                LOGGER.info(String.format("Queued message: %s. ClientId: %s", message, clientId));
                messagesToSend.offer(new Message(message, subject));
            }
        } finally {
            lock.unlock();
        }
    }

    public String readMessage() throws InterruptedException {
        final String message = readMessages.take();
        if (!message.equals(Resources.NULL_MESSAGE)) {
            return message;
        }
        return null;
    }

    public void close() {
        lock.lock();
        try {
            if (!closed) {
                closed = true;
                disconnect();
                readMessages.clear();
                messagesToSend.clear();
                masterSocket = null;
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * @return the local address of this endpoint.
     * @throws InvalidStateException if the endpoint is closed or is not connected
     */
    public SocketAddress getLocalAddress() throws InvalidStateException {
        lock.lock();
        try {
            if (!connected) {
                throw new IllegalStateException(
                        String.format("Failed to get local address. %s connected : %b",
                                this.getClass().getName(), connected)
                );
            }
            return masterSocket.getLocalAddress();
        } finally {
            lock.unlock();
        }
    }

    /**
     * @return the remote address of this endpoint.
     * @throws InvalidStateException if the endpoint is closed or is not connected
     */
    public SocketAddress getRemoteAddress() throws InvalidStateException {
        lock.lock();
        try {
            if (!connected) {
                throw new IllegalStateException(
                        String.format("Failed to get remote address. %s connected : %b",
                                this.getClass().getName(), connected)
                );
            }
            return masterSocket.getRemoteAddress();
        } finally {
            lock.unlock();
        }
    }

    public void addSocket(final Socket socket) {
        lock.lock();
        try {
            sockets.add(socket);
        } finally {
            lock.unlock();
        }

    }

    public void initIdleHandler(
            final AbstractIdleHandlerFactory idleHandlerFactory,
            final int timeout,
            final String clientId
    ) {
        final HeartbeatEventListener heartbeatEventListener = new HeartbeatEventListenerImpl(this);
        idleHandler = idleHandlerFactory.create(timeout, heartbeatEventListener, clientId);
    }

    /**
     * Called when a message is received on this endpoint.
     *
     * @param message the received message
     */
    public void notifyMessageReceived(final String message) {
        pushMessage(message);
    }

    private void pushMessage(final String message) {
        try {
            readMessages.put(message);
        } catch (InterruptedException e) {
            LOGGER.error("Failed to notify message received!", e);
        }
    }

    public void notifyConnectionEstablished() throws InvalidStateException {
        manageChannelHandlers();

        lock.lock();
        try {
            if (masterSocket != null) {
                messageDispatcher.start(masterSocket);
            }
            connected = true;
            Message message = messagesToSend.poll();
            while (message != null) {
                try {
                    messageDispatcher.sendMessage(message.getMessage(), message.getSubject());
                    LOGGER.info(String.format("Re-sent message: %s. ClientId: %s", message.getMessage(), clientId));
                    message = messagesToSend.poll();
                } catch (InvalidStateException e) {
                    LOGGER.error("Failed to send buffered messages after reconnect", e);
                }
            }
        } finally {
            lock.unlock();
        }
    }

    private void manageChannelHandlers() {
        // these operations will be performed only for multi-socket endpoints
        // (in case of single-socket endpoint, the socket list is empty so no operation will be performed)
        DelimiterHandler.destroyHandler(new ArrayList<Socket>(sockets));
        initCustomHandlers();
    }

    /**
     * Adds some handlers to the handler pipeline of the sockets
     */
    private void initCustomHandlers() {
        ArrayList<Socket> socketsCopy = new ArrayList<Socket>(sockets);
//        CompressionHandler.initHandler(socketsCopy);
        LengthFieldHandler.initHandler(socketsCopy);
        if (idleHandler != null) {
            idleHandler.initHandler(socketsCopy);
        }
    }

    public void disconnect() {
        lock.lock();
        try {
            connected = false;
            closeAndCleanUpSockets();
        } finally {
            lock.unlock();
        }
    }

    public boolean containsSocket(Socket socket) {
        lock.lock();
        try {
            return sockets.contains(socket);
        } finally {
            lock.unlock();
        }
    }

    private void closeAndCleanUpSockets() {
        closeSockets();
        sockets.clear();
        messageDispatcher.stop();
    }

    private void closeSockets() {
        for (com.bet.util.multisocket.common.Socket socket : sockets) {
            socket.close();
        }
        if (masterSocket != null) {
            masterSocket.close();   // for single-socket endpoints
        }
        LOGGER.info(String.format("Closed sockets on endpoint: %s", clientId));
    }

    /**
     * Adds a null message to read messages queue.
     * Client code will call close, when reads null.
     */
    public void fireClose() {
        LOGGER.info("Null message added to queue.");
        readMessages.offer(Resources.NULL_MESSAGE);
    }

    public boolean isConnected() {
        return connected;
    }

    public boolean isClosed() {
        return closed;
    }

    public boolean isEstablished() {
        return established;
    }

    public void setMasterSocket(Socket masterSocket) {
        this.masterSocket = masterSocket;
    }

    @Override
    public String toString() {
        return String.format("%s - %s", this.getClass(), clientId);
    }

    protected abstract T createEndpoint();

    public void connectionEstablished() {
        established = true;
        connectionEstablished(createEndpoint());
    }

    @Override
    public void connectionEstablished(T endpoint) {
        endpointListener.connectionEstablished(endpoint);
    }

    @Override
    public void connectionFailed() {
        endpointListener.connectionFailed();
    }

    @Override
    public void connectionClosed() {
        endpointListener.connectionClosed();
    }

    @Override
    public void reconnectionSuccessful() {
        endpointListener.reconnectionSuccessful();
    }

    @Override
    public void reconnectionFailed() {
        endpointListener.reconnectionFailed();
    }

    @Override
    public void notifyHeartbeatRecovered(String socketId, String clientId) {
        endpointListener.notifyHeartbeatRecovered(socketId, clientId);
    }

    @Override
    public void notifyHeartbeatLate(String socketId, String clientId) {
        endpointListener.notifyHeartbeatLate(socketId, clientId);
    }

    @Override
    public void notifyHeartbeatLost(String socketId, String clientId) {
        endpointListener.notifyHeartbeatLost(socketId, clientId);
    }

    @Override
    public void notifyHeartbeatDead(String socketId, String clientId) {
        endpointListener.notifyHeartbeatDead(socketId, clientId);
    }
}
