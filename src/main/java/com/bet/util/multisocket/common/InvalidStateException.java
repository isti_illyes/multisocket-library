package com.bet.util.multisocket.common;

/**
 * User: istvan.illyes
 * Date: 8/21/13
 */
public class InvalidStateException extends Exception {

    public InvalidStateException(final String message) {
        super(message);
    }

    public InvalidStateException(final String message, final Throwable throwable) {
        super(message, throwable);
    }
}
