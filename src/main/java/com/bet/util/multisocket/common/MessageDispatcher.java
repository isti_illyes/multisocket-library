package com.bet.util.multisocket.common;

import java.util.concurrent.locks.*;

import com.bet.blues.proxy.socket.multi.SubjectsDispatcher;
import io.netty.channel.ChannelFuture;
import org.apache.log4j.Logger;

/**
 * User: istvan.illyes
 * Date: 8/13/13
 */
public class MessageDispatcher {

    private static final Logger LOGGER = Logger.getLogger(MessageDispatcher.class.getName());
    private SubjectsDispatcher<Socket> subjectsDispatcher;
    private Socket masterSocket;
    private final Lock lock = new ReentrantLock();
    private volatile boolean started = false;

    // used for multi-socket endpoints
    public MessageDispatcher(SubjectsDispatcher<Socket> subjectsDispatcher) {
        this.subjectsDispatcher = subjectsDispatcher;
    }

    // used for single-socket endpoints
    public MessageDispatcher() {
    }

    public void sendMessage(String message, String subject) throws InvalidStateException {
        lock.lock();
        try {
            if (!started) {
                throw new InvalidStateException(
                        String.format("Failed to send message. %s started: %b", this.getClass().getName(), started)
                );
            }
            executeSend(message, subject);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Sends the message on the socket associated with the subject
     * given as parameter. If no socket is associated with the subject,
     * the message is sent on the masterSocket.
     *
     * @param message
     * @param subject
     * @return the {@link io.netty.channel.ChannelFuture}
     *         which will be notified when the writeAndFlush request succeeds or fails
     *         or {@code null} if session is not completely initialized
     */
    private ChannelFuture executeSend(String message, String subject) throws InvalidStateException {
        if (subjectsDispatcher != null) {
            Socket socket = subjectsDispatcher.getProcessor(subject);
            if (socket != null) {
                LOGGER.info(String.format("Message sent on socket: %s", socket));
                return socket.writeAndFlush(message);
            }
        }
        return sendMessageOnMasterSocket(message);
    }

    /**
     * Sends a message on the master socket.
     *
     * @param message
     * @return the {@link ChannelFuture} which will be notified when the
     *         writeAndFlush request succeeds or fails
     */
    private ChannelFuture sendMessageOnMasterSocket(String message) {
        LOGGER.info(String.format("Message sent on socket: %s", masterSocket));
        return masterSocket.writeAndFlush(message);
    }

    public void start(Socket masterSocket) throws InvalidStateException {
        lock.lock();
        try {
            if (started) {
                throw new InvalidStateException(String.format("Failed to start %s. Already started",
                        this.getClass().getName()));
            }
            started = true;
            this.masterSocket = masterSocket;
        } finally {
            lock.unlock();
        }
    }

    public void stop() {
        lock.lock();
        try {
            started = false;
            masterSocket = null;
            if(subjectsDispatcher != null) {
                subjectsDispatcher.clearProcessors();
            }
        } finally {
            lock.unlock();
        }
    }
}


