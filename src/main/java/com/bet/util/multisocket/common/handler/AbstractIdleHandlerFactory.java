package com.bet.util.multisocket.common.handler;

import com.bet.util.multisocket.common.heartbeat.HeartbeatEventListener;

/**
 * User: istvan.illyes
 * Date: 2/5/14
 */
public abstract class AbstractIdleHandlerFactory {

    public abstract AbstractIdleHandler create(
            final int timeoutMillis,
            final HeartbeatEventListener heartbeatEventListener,
            final String clientId
    );
}
