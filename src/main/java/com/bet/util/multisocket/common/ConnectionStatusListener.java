package com.bet.util.multisocket.common;

/**
 * User: istvan.illyes
 * Date: 1/21/14
 */
public interface ConnectionStatusListener {

    public void connectionFailed();

    public void connectionClosed();

    public void reconnectionSuccessful();

    public void reconnectionFailed();
}
