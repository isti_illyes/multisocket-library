package com.bet.util.multisocket.common.heartbeat;

import java.util.*;

/**
 * Implementation of {@code HeartbeatEventListener} which notifies the given
 * {@code HeartbeatEventListener}s when the heartbeat of a socket is late/lost/dead/recovered
 * <p/>
 * User: istvan.illyes
 * Date: 7/1/13
 */
public final class HeartbeatEventListenerImpl implements HeartbeatEventListener {

    private List<HeartbeatEventListener> listeners;

    public HeartbeatEventListenerImpl(final HeartbeatEventListener... listeners) {
        this.listeners = Arrays.asList(listeners);
    }

    public void notifyHeartbeatRecovered(String socketId, String clientId) {
        for (HeartbeatEventListener heartbeatEventListener : listeners) {
            heartbeatEventListener.notifyHeartbeatRecovered(socketId, clientId);
        }
    }

    public void notifyHeartbeatLate(String socketId, String clientId) {
        for (HeartbeatEventListener heartbeatEventListener : listeners) {
            heartbeatEventListener.notifyHeartbeatLate(socketId, clientId);
        }
    }

    public void notifyHeartbeatLost(String socketId, String clientId) {
        for (HeartbeatEventListener heartbeatEventListener : listeners) {
            heartbeatEventListener.notifyHeartbeatLost(socketId, clientId);
        }
    }

    public void notifyHeartbeatDead(String socketId, String clientId) {
        for (HeartbeatEventListener heartbeatEventListener : listeners) {
            heartbeatEventListener.notifyHeartbeatDead(socketId, clientId);
        }
    }
}
