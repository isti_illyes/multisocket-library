package com.bet.util.multisocket.common.handler;

import java.util.concurrent.atomic.*;

import com.bet.util.multisocket.common.heartbeat.HeartbeatEventListener;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;

/**
 * User: istvan.illyes
 * Date: 2/5/14
 */
public abstract class AbstractIdleChannelHandler extends ChannelDuplexHandler {

    private final String socketId;
    private final String clientId;
    private final IdleHandlerContext context;
    private final HeartbeatEventListener stateEvent;
    private final AtomicInteger readerTotalIdleTime = new AtomicInteger(0);

    public AbstractIdleChannelHandler(final HeartbeatEventListener stateEvent, final String socketId, final String clientId, final IdleHandlerContext context) {
        this.stateEvent = stateEvent;
        this.socketId = socketId;
        this.clientId = clientId;
        this.context = context;
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent e = (IdleStateEvent) evt;
            if (e.state().equals(IdleState.READER_IDLE)) {
                readerTotalIdleTime.addAndGet(context.getReaderIdleLimit());
                if (readerTotalIdleTime.get() >= context.getDeadTimeLimit()) {
                    stateEvent.notifyHeartbeatDead(socketId, clientId);

                } else if (readerTotalIdleTime.get() >= context.getLostTimeLimit()) {
                    stateEvent.notifyHeartbeatLost(socketId, clientId);

                } else if (readerTotalIdleTime.get() >= context.getLateTimeLimit()) {
                    stateEvent.notifyHeartbeatLate(socketId, clientId);
                }
            }
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (readerTotalIdleTime.get() >= context.getLateTimeLimit()) {
            stateEvent.notifyHeartbeatRecovered(socketId, clientId);
            readerTotalIdleTime.set(0);
        }
        ctx.fireChannelRead(msg);
    }
}
