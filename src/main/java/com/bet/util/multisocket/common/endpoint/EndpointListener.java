package com.bet.util.multisocket.common.endpoint;

import com.bet.util.multisocket.common.ConnectionStatusListener;
import com.bet.util.multisocket.common.heartbeat.HeartbeatEventListener;

/**
 * Created by isti on 1/12/14.
 */
public interface EndpointListener<T> extends HeartbeatEventListener, ConnectionStatusListener {

    public void connectionEstablished(T endpoint);
}
