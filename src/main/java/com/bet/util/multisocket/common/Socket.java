package com.bet.util.multisocket.common;

import java.net.*;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoop;

/**
 * User: istvan.illyes
 * Date: 7/1/13
 * <p/>
 * Wrapper class of a low-level connection.
 * The currently used low-level connection is {@link Channel}
 */
public class Socket {

    //The low-level connection
    private final Channel channel;
    private String socketId;

    public Socket(final Channel channel) {
        this.channel = channel;
    }

    /**
     * Sends a message on the low-level connection asynchronously.
     *
     * @param message the message to writeAndFlush
     * @return the {@link ChannelFuture} which will be notified when the
     *         writeAndFlush request succeeds or fails
     * @see Channel
     */
    public ChannelFuture writeAndFlush(final Object message) {
        return channel.writeAndFlush(message);
    }

    /**
     * Closes the low-level connection asynchronously.
     *
     * @return the {@link ChannelFuture} which will be notified when the
     *         close request succeeds or fails
     * @see Channel
     */
    public ChannelFuture close() {
        return channel.close();
    }

    /**
     * Returns the {@link ChannelPipeline} associated with the low-level connection.
     */
    public ChannelPipeline getPipeline() {
        return channel.pipeline();
    }

    /**
     * Returns the local address where the low-level connection is bound to. The returned
     * {@link SocketAddress} is supposed to be down-cast into more concrete
     * type such as {@link java.net.InetSocketAddress} to retrieve the detailed
     * information.
     *
     * @return the local address of the low-level connection.
     *         {@code null} if the low-level connection is not bound.
     */
    public SocketAddress getLocalAddress() {
        return channel.localAddress();
    }

    /**
     * Returns the remote address where the low-level connection is connected to.  The
     * returned {@link SocketAddress} is supposed to be down-cast into more
     * concrete type such as {@link java.net.InetSocketAddress} to retrieve the detailed
     * information.
     *
     * @return the remote address of the low-level connection.
     *         {@code null} if this channel is not connected.
     */
    public SocketAddress getRemoteAddress() {
        return channel.remoteAddress();
    }

    public EventLoop getEventLoop() {
        return channel.eventLoop();
    }

    @Override
    public boolean equals(final Object that) {
        if (!(that instanceof Socket)) {
            return false;
        }
        return channel.equals(((Socket) that).channel);
    }

    @Override
    public int hashCode() {
        return channel.hashCode();
    }

    @Override
    public String toString() {
        return String.format("%s %s", socketId, channel.toString());
    }

    public String getSocketId() {
        return socketId;
    }

    public void setSocketId(String socketId) {
        this.socketId = socketId;
    }
}
