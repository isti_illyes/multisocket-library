package com.bet.util.multisocket.common.endpoint;

import java.net.*;

import com.bet.util.multisocket.common.InvalidStateException;

/**
 * User: istvan.illyes
 * Date: 1/29/14
 */
public abstract class MultiSocketEndpoint {

    public abstract void sendMessage(String message, String subject) throws InvalidStateException;

    public abstract String readMessage() throws InterruptedException;

    public abstract void close();

    /**
     * @return the local address of this endpoint.
     * @throws InvalidStateException if the endpoint is closed or is not connected
     */
    public abstract SocketAddress getLocalAddress() throws InvalidStateException;

    /**
     * @return the remote address of this endpoint.
     * @throws InvalidStateException if the endpoint is closed or is not connected
     */
    public abstract SocketAddress getRemoteAddress() throws InvalidStateException;
}
