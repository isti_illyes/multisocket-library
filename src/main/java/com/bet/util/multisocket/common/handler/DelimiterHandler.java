package com.bet.util.multisocket.common.handler;

import java.util.*;

import com.bet.util.multisocket.common.Resources;
import com.bet.util.multisocket.common.Socket;
import io.netty.channel.ChannelHandler;
import org.apache.log4j.Logger;

/**
 * User: istvan.illyes
 * Date: 1/24/14
 */
public final class DelimiterHandler {

    private static final Logger LOGGER = Logger.getLogger(DelimiterHandler.class);

    /**
     * Removes {@code DelimiterBasedFrameDecoder} from the handler pipelines of the given sockets
     *
     * @param sockets - collection containing the {@code Socket}s
     */
    public static void destroyHandler(Collection<Socket> sockets) {
        Socket[] sktArray = new Socket[sockets.size()];
        sktArray = sockets.toArray(sktArray);

        for (Socket socket : sktArray) {
            try {
                if (socket != null) {
                    ChannelHandler ch = socket.getPipeline().remove(Resources.DELIMITER_DECODER_NAME);
                    LOGGER.info(String.format("Removed handler: %s", ch));
                }
            } catch (NoSuchElementException e) {
                LOGGER.warn(String.format("Failed to remove handler! Sockets: %s", (Object[])sktArray), e);
            }
        }
    }
}
