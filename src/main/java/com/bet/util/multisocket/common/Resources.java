package com.bet.util.multisocket.common;

import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.CharsetUtil;

/**
 * User: istvan.illyes
 * Date: 7/9/13
 */
public final class Resources {

    private Resources() {
    }

    // HANDLERS
    public static final StringEncoder STRING_ENCODER = new StringEncoder(CharsetUtil.UTF_8);
    public static final StringDecoder STRING_DECODER = new StringDecoder(CharsetUtil.UTF_8);
    public static final LoggingHandler LOGGING_HANDLER = new LoggingHandler();
    public static final String COMPRESSION_ENCODER_NAME = "compressionEncoder";
    public static final String COMPRESSION_DECODER_NAME = "compressionDecoder";
    public static final String STRING_ENCODER_NAME = "stringEncoder";
    public static final String STRING_DECODER_NAME = "stringDecoder";
    public static final String FRAME_ENCODER_NAME = "frameEncoder";
    public static final String FRAME_DECODER_NAME = "frameDecoder";
    public static final String DELIMITER_DECODER_NAME = "delimiterDecoder";
    public static final String LOGGING_HANDLER_NAME = "loggingHandler";
    public static final String CLIENT_HANDLER_NAME = "clientHandler";
    public static final String IDLE_STATE_HANDLER_NAME = "idleStateHandler";
    public static final String IDLE_HANDLER_NAME = "idleHandler";
    public static final String SERVER_HANDLER_NAME = "serverHandler";

    // SOCKET PROPERTIES
    // maximum queue length for incoming connection indications (a request to connect)
    public static final int SO_BACKLOG = 128;

    // FRAME OPTIONS
    public static final int FRAME_LENGTH_FIELD_OFFSET = 0;
    public static final int FRAME_LENGTH_ADJUSTMENT = 0;
    public static final int FRAME_LENGTH_FIELD_SIZE = 3;
    public static final int MAX_FRAME_LENGTH = (int) Math.pow(2, FRAME_LENGTH_FIELD_SIZE * 8);

    // MESSAGE HEADERS
    public static final String PING_MESSAGE_HEADER = "ping header";
    public static final String MASTER_SOCKET_CONNECT_MESSAGE_HEADER = "master connect header";
    public static final String MASTER_SOCKET_ID_MESSAGE_HEADER = "master id header";
    public static final String MASTER_SOCKET_ID_ACK_MESSAGE_HEADER = "master id ack header";
    public static final String SUBJECTS_ROUTING_MESSAGE_HEADER = "subjects routing header";
    public static final String CUSTOM_SOCKET_CONNECT_MESSAGE_HEADER = "custom connection header";
    public static final String ALL_CONNECTIONS_ESTABLISHED_MESSAGE_HEADER = "all connections header";
    public static final String MASTER_SOCKET_RECONNECT_MESSAGE_HEADER = "master reconnect header";
    public static final String MASTER_SOCKET_RECONNECT_ACK_MESSAGE_HEADER = "master reconnect ack header";

    // MESSAGE SEPARATORS
    public static final String MESSAGE_SEPARATOR = "\r\n";
    public static final String MESSAGE_FIELD_SEPARATOR = "~";

    // CUSTOM MESSAGES
    public static final String NULL_MESSAGE = "null message";

    // HEARTBEAT TIME LIMITS
    public static final double HEARTBEAT_LATE_MULTIPLIER = 2;
    public static final double HEARTBEAT_LOST_MULTIPLIER = 3;
    public static final double HEARTBEAT_DEAD_MULTIPLIER = 10;
    public static final double WRITER_IDLE_MULTIPLIER = 0.2;
    public static final double READER_IDLE_MULTIPLIER = 1;
    public static final int ALL_IDLE_TIME_LIMIT = 0;

    // SENDING PROPERTIES
    public static final int SEND_TIMEOUT_MILLIS = 2000;
    public static final int NO_SEND_RETRIES = 2;

    // RECONNECT PROPERTIES
    public static final int RECONNECT_TIMEOUT_MILLIS = 10000;
    public static final int MANUAL_RECONNECT_TIMEOUT_MILLIS = 3 * RECONNECT_TIMEOUT_MILLIS;
    public static final int NO_RECONNECT_RETRIES = 2;

    // OLD SOCKET PROPERTIES
    public static final String OLD_SOCKET_CLIENT_ID = "old_socket_clientId";
    public static final String EMPTY_SUBJECT = "";

    // SERVER PROPERTIES
    public static final long SERVER_CLEANUP_TIMEOUT_MILLIS = 60000;
    public static final long SERVER_ENDPOINT_CLOSE_TIMEOUT_MILLIS = 15000;
}
