package com.bet.util.multisocket.common.heartbeat;

/**
 * An interface through which the parties interested in handling heartbeat
 * events are notified of them.
 * <p/>
 * User: istvan.illyes
 * Date: 7/1/13
 */
public interface HeartbeatEventListener {

    /**
     * Notification sent when a heartbeat is late or lost and a message is received.
     */
    public void notifyHeartbeatRecovered(String socketId, String clientId);

    /**
     * Notification sent when the heartbeat is late, according to the timeout
     * setting in the idle handler.
     */
    public void notifyHeartbeatLate(String socketId, String clientId);

    /**
     * Notification sent when the heartbeat is lost, according to the timeout
     * setting in the idle handler.
     */
    public void notifyHeartbeatLost(String socketId, String clientId);

    /**
     * Notification sent when the heartbeat is dead, according to the timeout
     * setting in the idle handler. The listener can treat this dead
     * connection as unusable, and discard it.
     */
    public void notifyHeartbeatDead(String socketId, String clientId);
}