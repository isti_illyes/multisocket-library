package com.bet.util.multisocket.common.handler;

import java.util.*;
import java.util.concurrent.*;

import com.bet.util.multisocket.common.Resources;
import com.bet.util.multisocket.common.Socket;
import com.bet.util.multisocket.common.heartbeat.HeartbeatEventListener;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.timeout.IdleStateHandler;
import org.apache.log4j.Logger;

/**
 * User: istvan.illyes
 * Date: 7/30/13
 */
public class IdleHandler extends AbstractIdleHandler {

    private static final Logger LOGGER = Logger.getLogger(IdleHandler.class);

    /**
     * @param timeoutMillis          An integer value used to determine the limits for heartbeat events (late, lost, dead).
     * @param heartbeatEventListener The heartbeat event listener used by the idle handler
     * @param clientId
     * @return
     */
    protected IdleHandler(
            final int timeoutMillis,
            final HeartbeatEventListener heartbeatEventListener,
            final String clientId
    ) {
        super(timeoutMillis, heartbeatEventListener, clientId);
    }

    /**
     * Adds {@code IdleStateHandler} and {@code IdleChannelHandler} to the handler pipelines of the given sockets
     *
     * @param sockets - collection containing the {@code Socket}s
     */
    @Override
    public void initHandler(Collection<Socket> sockets) {
        for (Socket socket : sockets) {
            ChannelPipeline p = socket.getPipeline();
            if (p.get(Resources.IDLE_HANDLER_NAME) == null && p.get(Resources.IDLE_STATE_HANDLER_NAME) == null) {
                p.addAfter(Resources.STRING_DECODER_NAME, Resources.IDLE_HANDLER_NAME,
                        new IdleChannelHandler(
                                getHeartbeatEventListener(), socket.getSocketId(),
                                getClientId(), getIdleHandlerContext()
                        )
                );
                p.addAfter(Resources.STRING_DECODER_NAME, Resources.IDLE_STATE_HANDLER_NAME,
                        new IdleStateHandler(getIdleHandlerContext().getReaderIdleLimit(),
                                getIdleHandlerContext().getWriterIdleLimit(),
                                getIdleHandlerContext().getAllIdleLimit(), TimeUnit.MILLISECONDS
                        )
                );

            } else {
                LOGGER.warn(String.format("Failed to initialize %s and %s. Already initialized.",
                        Resources.IDLE_HANDLER_NAME, Resources.IDLE_STATE_HANDLER_NAME));
            }
        }
    }
}
