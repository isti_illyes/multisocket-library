package com.bet.util.multisocket.common;

/**
 * User: istvan.illyes
 * Date: 7/29/13
 * <p/>
 * Enum used to create a SocketChannelFactory.
 */

public enum ChannelType {

    // New IO - non-blocking
    NIO,

    // Old IO - blocking
    OIO;

    public static ChannelType create(final String channelType) {
        if (channelType.equals("OIO")) {
            return OIO;

        } else if (channelType.equals("NIO")) {
            return NIO;

        } else {
            return OIO;
        }
    }
}
